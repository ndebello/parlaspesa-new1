//
//  TABButtonObject.h
//  ParlaSpesa
//
//  Created by Leonardo Amico on 22/01/15.
//
//

#ifndef __ParlaSpesa__TABButtonObject__
#define __ParlaSpesa__TABButtonObject__

#include "TABInteractiveObject.h"

class TABButtonObject: public TABInteractiveObject{
    
public:
    TABButtonObject(); 
    TABButtonObject(string _imgPath, ofPoint centerPos); 
    bool hit(ofPoint touchPt);
    bool loadSoundfile(string soundfileName);

protected:
    ofSoundPlayer soundPlayer;
    
    
};
#endif /* defined(__ParlaSpesa__TABButtonObject__) */
