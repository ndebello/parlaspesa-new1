#import "ofAppDelegate.h"

#import <DropboxSDK/DropboxSDK.h>

@interface ofAppDelegate() <DBSessionDelegate, DBNetworkRequestDelegate>


@end

@implementation ofAppDelegate

@synthesize navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [super applicationDidFinishLaunching:application];
        
    
    // using a storyboard to layout the scenes
    // eg. intro screen, info screen, game screen, and end screen
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    navigationController = [storyboard instantiateInitialViewController];
    [self.window setRootViewController:self.navigationController];
    
    [self setupDB]; 
    
    return YES;
}

// drop in replacement for ofxiOSGetViewController() as glViewController = nil
- (UIViewController*) getViewController {
    return [self.navigationController visibleViewController];
}

// overriding for rotation because alerts do not go through when using custom view controller.
- (void)receivedRotate:(NSNotification*)notification {
    
    //[super receivedRotate:notification];
	UIDeviceOrientation interfaceOrientation = [[UIDevice currentDevice] orientation];
    ofLogVerbose("AppDelegate/ofxiOSAppDelegate") << "device orientation changed to " << interfaceOrientation;
	if(interfaceOrientation != UIDeviceOrientationUnknown) {
        ofxiOSAlerts.deviceOrientationChanged(interfaceOrientation);
    }
}

#pragma mark - dropbox
- (void) setupDB{
    // Set these variables before launching the app
    NSString* appKey = @"8te7fuj7krp9hax";
    NSString* appSecret = @"vq499klnrvm4mae";
    NSString *root = kDBRootAppFolder;
    
    if ([appKey rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location != NSNotFound) {
        NSLog(@"DB setup - Make sure you set the app key correctly in AppDelegate.m");
    } else if ([appSecret rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]].location != NSNotFound) {
        NSLog(@"DB setup - Make sure you set the app secret correctly in AppDelegate.m");
    } else if ([root length] == 0) {
        NSLog(@"DB setup - Set your root to use either App Folder of full Dropbox");
    } else {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
        NSData *plistData = [NSData dataWithContentsOfFile:plistPath];
        NSDictionary *loadedPlist =
        [NSPropertyListSerialization
         propertyListFromData:plistData mutabilityOption:0 format:NULL errorDescription:NULL];
        NSString *scheme = [[[[loadedPlist objectForKey:@"CFBundleURLTypes"] objectAtIndex:0] objectForKey:@"CFBundleURLSchemes"] objectAtIndex:0];
        if ([scheme isEqual:appKey]) {
            NSLog(@"DB setup - Set your URL scheme correctly in Info.plist (needs to be written as db-APP_KEY");
        }
    }
    
    DBSession *dbSession = [[DBSession alloc]
                            initWithAppKey:appKey
                            appSecret:appSecret
                            root:root]; // either kDBRootAppFolder or kDBRootDropbox
    [DBSession setSharedSession:dbSession];
    
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url
  sourceApplication:(NSString *)source annotation:(id)annotation {
    if ([[DBSession sharedSession] handleOpenURL:url]) {
        if ([[DBSession sharedSession] isLinked]) {
            NSLog(@"App linked successfully!");
            // At this point you can start making API calls
        }
        return YES;
    }
    // Add whatever other url handling code your app requires here
    return NO;
}



@end
