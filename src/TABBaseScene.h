//
//  TABBaseScene.h
//  TABLEApp
//
//  Created by Leonardo Amico on 22/09/14.
//
//

#ifndef __TABLEApp__TABBaseScene__
#define __TABLEApp__TABBaseScene__

#include "ofMain.h"

#include "defines.h"


class TABBaseScene
{
public:
    TABBaseScene()
    {
        bDead = false;
        bCompleted = false;
        bDebug = false;
        
        //
        bIntroTransitionFade = true;
        bOutroTransitionFade = true;
    }
    
    virtual void initIntro(){};
    virtual void initOutro(){};
    
    virtual void update(){};
    virtual void draw(){};
    
    virtual void touchDown(ofPoint touchPt){};
    virtual void touchMoved(ofPoint touchPt){};
    virtual void touchUp(ofPoint touchPt){};
    
    //
    bool bDebug;

    
    bool isDead()
    {
        return bDead; 
    }
    bool isCompleted()
    {
        return bCompleted; 
    }
    
    string getKind()
    {
        return sceneKind;
    }
    
    TABBaseScene* getScene()
    {
        return this;
    }
        
    
protected:
    virtual void intro(){};
    virtual void outro(){};
    
    bool bDead;
    bool bCompleted;
    string sceneKind;
    
    bool bIntroTransitionFade; //If true transition is a fade if false it's a slide
    bool bOutroTransitionFade; //If true transition is a fade if false it's a slide


};

#endif /* defined(__TABLEApp__TABBaseScene__) */
