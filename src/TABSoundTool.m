//
//  TABSoundTool.m
//  TABLEApp
//
//  Created by Leonardo Amico on 10/10/14.
//
//

#import "TABSoundTool.h"

@interface TABSoundTool()

@property BOOL bPlaying;
@property BOOL bRecording;
@property AVAudioPlayer* soundPlayer;
@property AVAudioRecorder* soundRecorder;
@property BOOL bDetectStartRecording;
@property BOOL bDetectStopRecording;
@end

@implementation TABSoundTool

- (instancetype)init{
    self = [super init];
    if (self){
        self.bRecording = NO;
        self.bPlaying = NO;
        self.bDetectStartRecording = NO;
        self.bDetectStopRecording = NO;
        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: nil];
    }
    return self;
}

- (BOOL) play:(NSString*)soundfileName isProductionSoundfile:(BOOL)bProductionSoundFile{
    
    if (self.bRecording)
    {
        [self.soundRecorder stop];
        self.bRecording = false;
    }
    
    //TEST
    [self.soundPlayer stop];
//    [self.soundPlayer st]
    self.soundPlayer = nil;
    BOOL ssuccess = [[AVAudioSession sharedInstance] setActive:NO error:nil];
    
    NSError *activationError = nil;
    BOOL success = [[AVAudioSession sharedInstance] setActive: YES error: &activationError];
    if (!success) {
        /* handle the error in activationError */
        return false;
    }
    
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &activationError];
    if (activationError != nil){
        NSLog([activationError description]);
        return false;
    }
    
    AVAudioPlayer* newPlayer;
    if (bProductionSoundFile)
        newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[self getSoundFileURL:soundfileName]
                                           error:&activationError];
    else
        newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[[NSURL alloc] initFileURLWithPath:soundfileName]
                                                                          error:&activationError];
        
    if (activationError != nil){
        NSLog([activationError description]);
        return false;
    }
     //TODO - handle error
    
    self.soundPlayer = newPlayer;
    [self.soundPlayer setDelegate:self];
    [self.soundPlayer play];
    [self.soundPlayer setMeteringEnabled:YES];
    
    self.bPlaying = true;
    
    return YES; 
    
}

- (BOOL)record:(NSString*)productionSoundfileName {
    
    if (self.bPlaying)
    {
        [self.soundPlayer stop];
        self.bPlaying = false;
    }
    
    
    NSError *activationError = nil;
    BOOL success = [[AVAudioSession sharedInstance] setActive: YES error: &activationError];
    if (!success) {
        /* handle the error in activationError */
        return false;
    }
    
    success = [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryRecord error: &activationError];
    if (!success) {
        /* handle the error in activationError */
        return false;
    }
    
    NSDictionary *recordSettings =
    [[NSDictionary alloc] initWithObjectsAndKeys:
     [NSNumber numberWithFloat: 44100.0], AVSampleRateKey,
     [NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
     [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
     [NSNumber numberWithInt: AVAudioQualityMax],
     AVEncoderAudioQualityKey,
     nil];
    
    AVAudioRecorder *newRecorder = [[AVAudioRecorder alloc] initWithURL: [self getSoundFileURL:productionSoundfileName]
                                settings: recordSettings
                                   error: &activationError];
    //TODO - handle error
    
    //
    self.soundRecorder = newRecorder;
    [self.soundRecorder setDelegate:self];
    [self.soundRecorder prepareToRecord];
    [self.soundRecorder record];
    [self.soundRecorder setMeteringEnabled:YES];
    
    
    self.bRecording = YES;
    
    
    return YES;
}

- (BOOL)stop {
    //Stop everything
    
    if (self.bRecording)
    {
        [self.soundRecorder stop];
        self.soundRecorder = nil;
        
        //Apparently OF can activate the audio session but not change its category. So after recording it's necessary to put it back to the playback category
        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: nil];
    }
    
    if (self.bPlaying){
        [self.soundPlayer stop];
        self.soundPlayer = nil;
    }
    
    self.bRecording = NO;
    self.bPlaying = NO;
    NSError *activationError = nil;
    BOOL success = [[AVAudioSession sharedInstance] setActive: NO error: &activationError];
    if (!success) {
        /* handle the error in activationError */
    }
    
    return YES;
    
}

- (NSURL* )getSoundFileURL:(NSString*)productionSoundfileName{
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *soundFilePath = [documentDirectory stringByAppendingPathComponent:productionSoundfileName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:soundFilePath]){
        //TODO - if existing the file is overwritten
        //Do something?!
    }
    
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    return newURL;
}

- (BOOL)isPlaying{
    return self.bPlaying;
}

- (BOOL)isRecording{
    return self.bRecording; 
}

- (float)getSoundLevel{
    float soundLevel = -160;
    if (self.bRecording){
        [self.soundRecorder updateMeters];
        soundLevel = [self.soundRecorder averagePowerForChannel:0];
    }else if (self.bPlaying){
        [self.soundPlayer updateMeters];
        [self.soundPlayer numberOfChannels];
        soundLevel = [self.soundPlayer averagePowerForChannel:0];
    }
    return soundLevel;
}
- (void) updateRecordDetector{
    [self.soundRecorder updateMeters];
    if (self.bRecording){
        float soundLevelAvg = [self.soundRecorder averagePowerForChannel:0];
        float soundLevelPeak = [self.soundRecorder peakPowerForChannel:0];

        if (!self.bDetectStartRecording && soundLevelPeak > -20 && soundLevelPeak - soundLevelAvg < 10){
//            NSLog(@"START\n");
            self.bDetectStartRecording = YES;
        }
        if (self.bDetectStartRecording && soundLevelPeak - soundLevelAvg > 25){
//            NSLog(@"STOP\n");
            self.bDetectStartRecording = NO;
            self.bDetectStopRecording = YES;
        }
    }
}

- (BOOL)getStopRecordingDetected{
    return self.bDetectStopRecording;
}

- (void)resetRecordDetector{
    self.bDetectStartRecording = NO;
    self.bDetectStopRecording = NO;
}

- (void)requestRecordPermission{
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            NSLog(@"granted"); //Do nothing
        } else {
            NSLog(@"denied"); //Pop up an alert
            
            // We're in a background thread here, so jump to main thread to do UI work.
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"L'app non ha accesso al microfono"
                                             message:@"Per utilizzare l'applicazione devi garantirgli accesso al microfono in Impstazioni > Sicurezza > Microfono"
                                            delegate:nil
                                   cancelButtonTitle:@"Ok"
                                   otherButtonTitles:nil] show];
            });
        }
    }];
}



@end
