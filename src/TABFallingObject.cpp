//
//  TABFallingObject.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 14/11/14.
//
//

#include "TABFallingObject.h"

TABFallingObject::TABFallingObject(string imgPath, ofPoint centerPos):TABInteractiveObject(imgPath, centerPos)
{
    
    speed = ofRandom(0.5, 8);
    x = ofRandom(0,1)*ofGetWidth();
    y = -ofRandom(0,1)*200 - height;
}

void TABFallingObject::update(){
    y+=speed;
    if (y > ofGetHeight()){
        y = - height;
        x = ofRandom(0,1)*ofGetWidth();
        speed = ofRandom(0.5, 8);
    }
        
}
