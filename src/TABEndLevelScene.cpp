//
//  TABEndLevelScene.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 16/10/14.
//
//

#include "TABEndLevelScene.h"

#include "TABImagesData.h"
#include "ofxXmlSettings.h"

TABEndLevelScene::TABEndLevelScene()
{
    sceneKind = "End Level";
    currentState = STATE_END_LEVEL_IDLE;
    endLevelImagesPos.resize(0);
    
    
    
    ofxXmlSettings gameXml;
    gameXml.load("GameData.xml");
    gameXml.pushTag("AdditionalSceneSettings");
    gameXml.pushTag("EndLevelScene");
    
    TABImagesData imagesData;
    bgImageId =  imagesData.getImageId(gameXml.getValue("Bg", ""));
    
    gameXml.pushTag("Level1");
    levelMedals.push_back(TABInteractiveObject(gameXml.getValue("Img",""),ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight())));
//    levelMedals[levelMedals.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
    gameXml.pushTag("Level2");
    levelMedals.push_back(TABInteractiveObject(gameXml.getValue("Img",""), ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight())));
//    levelMedals[levelMedals.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
    gameXml.pushTag("Level3");
    levelMedals.push_back(TABInteractiveObject(gameXml.getValue("Img",""), ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight())));
//    levelMedals[levelMedals.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
    gameXml.pushTag("Level4");
    levelMedals.push_back(TABInteractiveObject(gameXml.getValue("Img",""), ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight())));
//    levelMedals[levelMedals.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
//    gameXml.pushTag("Level5");
//    levelMedals.push_back(TABInteractiveObject(gameXml.getValue("Img",""), ofPoint(gameXml.getValue("xRelPos",0)*ofGetWidth(),gameXml.getValue("yRelPos",0)*ofGetHeight())));
//    levelMedals[levelMedals.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
//    gameXml.popTag();
    
    gameXml.pushTag("ContinueButton");
    continueButton = TABInteractiveObject(gameXml.getValue("Img", ""), ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    continueButton.setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight())); 
    gameXml.popTag();
    
    gameXml.popTag(); //pop End Level Scene
    gameXml.popTag(); //pop Additional Images

    
    bSceneExited = false;
    
    ofSetCircleResolution(255);

}
void TABEndLevelScene::intro(){
    if (sceneTween.isCompleted())
        initAnimation(); 
}
void TABEndLevelScene::outro(){
    if (sceneTween.isCompleted()){
        bDead = true;
        if (soundPlayer.getIsPlaying())
            soundPlayer.stop();
    }
    
    
    
}
void TABEndLevelScene::initIntro(){
    currentState = STATE_END_LEVEL_INTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABEndLevelScene::initOutro(){
    bCompleted = false;
    currentState = STATE_END_LEVEL_OUTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABEndLevelScene::update(){
    
    if (currentState == STATE_END_LEVEL_INTRO)
    {
        this->intro();
        
    }
    else if (currentState == STATE_END_LEVEL_ANIMATION)
    {
        this->animation();
        
    }
    else if (currentState == STATE_END_LEVEL_OUTRO){
        this->outro();
    }
}

void TABEndLevelScene::draw(){
    
    ofPushMatrix();
    ofPushStyle();
    
    if (currentState == STATE_END_LEVEL_INTRO){
        if (bIntroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*sceneTween.update());
        }else{
            float xTranslate = ofGetWidth() - sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
    }else if (currentState == STATE_END_LEVEL_OUTRO){
        if (bOutroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*(1 -sceneTween.update()));
        }else{
            float  xTranslate = -sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
    }

    
    TABImagesData::images[bgImageId].draw(0, 0); 
    
    
    float intPart;
    bool bShow = modf(objectsTween.update(), &intPart)>0.5?true:false;
    //Draw past levels
    for (int i=0; i<idLevel; i++){
        levelMedals[i].draw();
    }
    
    //Flashing new level - if object tween's not completed
    if (currentState != STATE_END_LEVEL_INTRO){
        if (objectsTween.isCompleted()){
            levelMedals[idLevel].draw();
            
        }else{
            if (bShow)
                levelMedals[idLevel].draw();
        }
    }
    
    if (!bEndGame){
        
//        for (int i=0; i<endLevelImagesPos.size(); i++){
//            ofPoint imgPosOff = ofPoint(TABImagesData::images[medalImgId].width/2, TABImagesData::images[medalImgId].height/2);
//            ofPoint pos = endLevelImagesPos[i] - imgPosOff;
//            TABImagesData::images[medalImgId].draw(pos);
//        }
    }
    else{
        
        for (int i=0; i<fallingObjects.size(); i++){
            fallingObjects[i].draw();
        }
        
        
//        ofPoint imgPosOff = ofPoint(TABImagesData::images[medalImgId].width/2, TABImagesData::images[medalImgId].height/2);
//        ofPoint pos = ofPoint(ofGetWidth()/2, ofGetHeight()/2) - imgPosOff;
//        TABImagesData::cupImage.draw(pos);

    }
    
    continueButton.draw();
    
    ofPopStyle();
    ofPopMatrix();

    
}

void TABEndLevelScene::initAnimation(){
    currentState = STATE_END_LEVEL_ANIMATION;
    
    
    if (!bEndGame){
        soundPlayer.loadSound("Sounds/FrAc_endLevel.mp3"); //TODO - 
    }
    else{
        soundPlayer.loadSound("Sounds/FrAc_finalVictory.mp3");
    }
    objectsTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 8, END_LEVEL_ANIMATION_DURATION, 0);
    soundPlayer.play();
    
}

void TABEndLevelScene::animation(){
    objectsTween.update();
    if (objectsTween.isCompleted()){
//        bCompleted = true;
    }
    
    if (bEndGame){
        for (int i=0; i<fallingObjects.size(); i++){
            fallingObjects[i].update();
        }
    }
    
}

void TABEndLevelScene::setLevel(int _idLevel, bool _bEndGame){
    idLevel = _idLevel;
    bEndGame = _bEndGame;
    
    if (!bEndGame) {
        int frac = idLevel+2;
        for (int i =1; i<frac; i++){
            int mul = i;
            
            endLevelImagesPos.push_back(ofPoint(ofGetWidth()*mul/frac,ofGetHeight()/2));
        }
    }else{
        ofxXmlSettings gameXml;
        gameXml.load("GameData.xml");
        gameXml.pushTag("AdditionalSceneSettings");
        gameXml.pushTag("EndLevelScene");
        gameXml.pushTag("GoldRain");
        
        //Double objects
        for (int i=0; i<gameXml.getNumTags("Object"); i++){
            fallingObjects.push_back(TABFallingObject(gameXml.getValue("Object","",i), ofPoint(0,0)));
        }
        for (int i=0; i<gameXml.getNumTags("Object"); i++){
            fallingObjects.push_back(TABFallingObject(gameXml.getValue("Object","",i), ofPoint(0,0)));
        }
        
        gameXml.popTag();
        gameXml.popTag();
        gameXml.popTag();
    
    }
}

void TABEndLevelScene::touchDown(ofPoint touchPt){
    
    if (continueButton.hit(touchPt)){
    }

}
void TABEndLevelScene::touchMoved(ofPoint touchPt){
    
}
void TABEndLevelScene::touchUp(ofPoint touchPt){
    
    if (!bSceneExited){
        if (continueButton.bDragged){
            continueButton.released(touchPt);
            if (!continueButton.bDragged){
                bCompleted = true;
                bSceneExited = true;
            }
        }
    }
    
}