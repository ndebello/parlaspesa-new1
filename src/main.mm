#include "ofMain.h"
#include "ofAppiOSWindow.h"
//#include "ofApp.h"

//Dirty hack from here http://forum.openframeworks.cc/t/xcode6-beta-of-0-8-1-for-ios-8-0/16103/8
extern "C"{
    size_t fwrite$UNIX2003( const void *a, size_t b, size_t c, FILE *d )
    {
        return fwrite(a, b, c, d);
    }
    char* strerror$UNIX2003( int errnum )
    {
        return strerror(errnum);
    }
    time_t mktime$UNIX2003(struct tm * a)
    {
        return mktime(a);
    }
    double strtod$UNIX2003(const char * a, char ** b) {
        return strtod(a, b);
    }
}

int main(){

    ofAppiOSWindow *iOSWindow = new ofAppiOSWindow();
    //iOSWindow->enableDepthBuffer();
    //iOSWindow->enableAntiAliasing(4);
    //iOSWindow->enableRendererES2();
     ofSetupOpenGL(ofPtr<ofAppiOSWindow>(iOSWindow), 1024,768, OF_FULLSCREEN);
        
    if(iOSWindow->isRetinaSupportedOnDevice()) iOSWindow->enableRetina();
    iOSWindow->enableHardwareOrientation();
    iOSWindow->enableOrientationAnimation();
    
    
    
   
    iOSWindow->startAppWithDelegate("ofAppDelegate");
    
    // old way
    //ofSetupOpenGL(1024,768, OF_FULLSCREEN);
    //ofRunApp(new ofApp());
    
   
}
