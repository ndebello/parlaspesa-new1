//
//  TABMenuScene.h
//  TABLEApp
//
//  Created by Leonardo Amico on 05/11/14.
//
//

#ifndef __TABLEApp__TABMenuScene__
#define __TABLEApp__TABMenuScene__

#include "TABBaseScene.h"
#include "TABButtonObject.h"
#include "TABToggleButtonObject.h"
#include "ofxXmlSettings.h"
#include "ofxTween.h"


#define STATE_MENU_INTRO                         "MENU - INTRO"
#define STATE_MENU_OUTRO                         "MENU - OUTRO"
#define STATE_MENU_IDLE                          "MENU - IDLE"

class TABMenuScene: public TABBaseScene
{
public: 
    TABMenuScene(); 
    
    //General scene functions
    virtual void update();
    virtual void draw();
    
    virtual void initIntro();
    virtual void initOutro();
    
    virtual void touchDown(ofPoint touchPt);
    virtual void touchMoved(ofPoint touchPt);
    virtual void touchUp(ofPoint touchPt);
    
    //
    string getSelectedButton();
    int getGender();
    void resetSelectedButton();
    void initOutroToResults();
    
    
private:
    virtual void intro();
    virtual void outro();
    
    TABButtonObject startButton;
    TABButtonObject resultsButton;
    TABButtonObject creditsButton;
    TABToggleButtonObject maleButton;
    TABToggleButtonObject femaleButton;
    
    
    vector <TABInteractiveObject> objectsCarousel;
    
    int bgImageId;
    
    
    string currentState; 
    
    
    ofxTween sceneTween;
    ofxEasingLinear easinglinear;
    
    string selectedButton;
    
    
};

#endif /* defined(__TABLEApp__TABMenuScene__) */
