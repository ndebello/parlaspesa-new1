//
//  TABLevelMenuScene.h
//  TABLEApp
//
//  Created by Leonardo Amico on 08/10/14.
//
//

#ifndef __TABLEApp__TABLevelMenuScene__
#define __TABLEApp__TABLevelMenuScene__

#define STATE_LEVEL_MENU_INTRO                                  "LEVEL_MENU - INTRO"
#define STATE_LEVEL_MENU_RESTART_INTRO                          "LEVEL_MENU - RESTART_INTRO"
#define STATE_LEVEL_MENU_OUTRO                                  "LEVEL_MENU - OUTRO"
#define STATE_LEVEL_MENU_IDLE                                   "LEVEL_MENU - IDLE"



#include "TABBaseScene.h"
#include "TABLevelIconObject.h"
#include "ofxTween.h"
#include "ofxXmlSettings.h"

class TABLevelMenuScene: public TABBaseScene
{
public:
    TABLevelMenuScene(int _nLevels, int _lastLevelAvailable);
    TABLevelMenuScene(int _nLevels);
    void construct(int _nLevels, int _lastLevelAvailable);
    
    //General scene functions
    virtual void update();
    virtual void draw();
    
    virtual void initIntro();
    virtual void initOutro();
    
    virtual void touchDown(ofPoint touchPt);
    virtual void touchMoved(ofPoint touchPt);
    virtual void touchUp(ofPoint touchPt);
    
    int getSelectedLevel(); 
protected:
    virtual void intro();
    virtual void outro();
    
    void initRestartIntro();
    
    string currentState; 
    vector<TABLevelIconObject> levelIcons;
    int selectedLevel;
    
    //
    ofxTween sceneTween;
    ofxEasingLinear easinglinear;
    
    int lastLevelAvailable;
    int nLevels;
    
    void loadLevelIcons();
    
    bool bLevelSelected;
    
    int bgImageId; 

};

#endif /* defined(__TABLEApp__TABLevelMenuScene__) */
