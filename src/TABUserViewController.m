//
//  TABUserViewController.m
//  ParlaSpesa
//
//  Created by Leonardo Amico on 10/03/15.
//
//

#import "TABUserViewController.h"
#import "TABTestViewController.h"
#import "TABSoundTool.h"


@interface TABUserViewController ()

@property TABSoundTool* soundTool;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *userInfo;


@end

@implementation TABUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.soundTool = [[TABSoundTool alloc] init];
    NSString* userInfoText = [NSString stringWithFormat:@"%@, %@ anni", [self.userData name], [self.userData age]];
    [[self userInfo] setText:userInfoText];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.userData.tests count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TestCell" forIndexPath:indexPath];
    
    
    NSArray* tests = [self.userData tests];
    NSString* testId = [tests[indexPath.row] testId];
    NSString* levelId = [tests[indexPath.row] levelId];
    NSString* date = [(TABTestDetailData*)tests[indexPath.row] time];

    
    NSArray *cellSubviews = [cell.contentView subviews];
    for (UIView* subview in cellSubviews){
        
        if ([subview tag] == 0){
            UILabel* dateLabel = (UILabel*)subview;
            [dateLabel setText:date];
        }else if ([subview tag] == 1){
            UILabel* levelLabel = (UILabel*)subview;
            [levelLabel setText:levelId];
        }else if ([subview tag] == 2){
            UILabel* testLabel = (UILabel*)subview;
            [testLabel setText:testId];
        }
    }

    return cell;
    
}


#pragma mark - Navigation
 
 

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showTestDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        TABTestDetailData* test = self.userData.tests[indexPath.row];
        TABTestViewController* testViewController = (TABTestViewController*)[segue destinationViewController];
        [testViewController setTest:test];
        [testViewController setUserData:self.userData];
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    //Reproduce button sound
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"buttonClick"
                                                         ofType:@"mp3"];
    [self.soundTool play:filePath isProductionSoundfile:NO];
}

- (IBAction)unwindToUser:(UIStoryboardSegue *)segue{
}

@end
