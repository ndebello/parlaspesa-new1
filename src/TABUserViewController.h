//
//  TABUserViewController.h
//  ParlaSpesa
//
//  Created by Leonardo Amico on 10/03/15.
//
//

#import <UIKit/UIKit.h>
#import "TABUserDetailsData.h"


@interface TABUserViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property TABUserDetailsData* userData;


@end
