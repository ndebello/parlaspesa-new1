//
//  TABButtonObject.cpp
//  ParlaSpesa
//
//  Created by Leonardo Amico on 22/01/15.
//
//

#include "TABButtonObject.h"

TABButtonObject::TABButtonObject(){}
TABButtonObject::TABButtonObject(string _imgPath, ofPoint centerPos):TABInteractiveObject(_imgPath,centerPos){}

bool TABButtonObject::hit(ofPoint touchPt){
    if (inside(touchPt)) {
        bDragged = true;
        touchAnchorOffset.x = touchPt.x - x;
        touchAnchorOffset.y = touchPt.y - y;
        if (soundPlayer.isLoaded())
            soundPlayer.play();
    }
    return bDragged;
}

bool TABButtonObject::loadSoundfile(string soundfileName){
    return soundPlayer.loadSound(soundfileName);
}