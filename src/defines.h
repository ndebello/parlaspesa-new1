//
//  defines.h
//  TABLEApp
//
//  Created by Leonardo Amico on 23/09/14.
//
//

#ifndef TABLEApp_defines_h
#define TABLEApp_defines_h


#define COMPRENSION_RESULT_FIRST_ATTEMPT        0
#define COMPRENSION_RESULT_SECOND_ATTEMPT       1
#define COMPRENSION_RESULT_FAILED               2
#define COMPRENSION_REPEAT_OBJECT_INTERVAL      10000

#define INTRO_OUTRO_SCENE_DURATION              1000
#define OBJECT_ANIMATION_DURATION               1000
#define INTRO_ANIMATION_DURATION                4000

#define END_LEVEL_ANIMATION_DURATION            2000

#define END_GAME_ANIMATION_DURATION             12000

#define PRODUCTION_RECORDING_TIME               6000

#define GENDER_MALE                             1
#define GENDER_FEMALE                           0



#endif
