//
//  TABLevelIconObject.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 09/10/14.
//
//

#include "TABLevelIconObject.h"

TABLevelIconObject::TABLevelIconObject(string imgPath, ofPoint centerPos, bool _bActive):TABButtonObject(imgPath, centerPos)
{
    bActive = _bActive;
}

void TABLevelIconObject::draw()
{
    ofPushStyle();
    if (bActive)
        TABImagesData::images[imageId].draw(x,y);
    ofPopStyle();
}
