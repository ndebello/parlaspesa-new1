//
//  TABProductionScene.h
//  TABLEApp
//
//  Created by Leonardo Amico on 22/09/14.
//
//

#ifndef __TABLEApp__TABProductionScene__
#define __TABLEApp__TABProductionScene__

#include "TABBaseScene.h"
#include "TABInteractiveObject.h"
#include "ofxTween.h"
#include "ofxXmlSettings.h"


#define STATE_PRODUCTION_INTRO                          "PRODUCTION - INTRO"
#define STATE_PRODUCTION_INITIAL_INSTRUCTIONS           "PRODUCTION - INITIAL_INSTRUCTIONS"
#define STATE_PRODUCTION_INSTRUCTIONS                   "PRODUCTION - INSTRUCTIONS"

#define STATE_PRODUCTION_OBJECT_OUT_CART                "PRODUCTION - OBJECT OUT CART"
#define STATE_PRODUCTION_OBJECT_AWAY                    "PRODUCTION - OBJECT AWAY"
#define STATE_PRODUCTION_OUTRO                          "PRODUCTION - OUTRO"
#define STATE_PRODUCTION_IDLE                           "PRODUCTION - IDLE"

#define STATE_PRODUCTION_CART_TO_BELT                   "PRODUCTION - CART_TO_BELT"
#define STATE_PRODUCTION_BELT_TO_SCANNER                "PRODUCTION - BELT_TO_SCANNER"
#define STATE_RECORD_VOICE_SIGNAL                       "PRODUCTION - RECORD_VOICE_SIGNAL"

#define STATE_RECORD_VOICE                              "PRODUCTION - RECORD VOICE"
#define STATE_PLAY_OBJECT_NAME                          "PRODUCTION - SAY OBJECT"
#define STATE_PRODUCTION_SCANNER_TO_AWAY                "PRODUCTION - SCANNER_TO_AWAY"


/*
 1. Object goes from cart to beginning of belt.
 2. Object goes from beginning of belt to scanner.
 3. Ballon appears and the voice is recorded.
 4. The baloon disappears and a voice recording tells the name of the object.
 5. The object goes away.
 */

class TABProductionScene: public TABBaseScene
{
public:
    TABProductionScene();
    
    //General scene functions
    virtual void update();
    virtual void draw();
    
    virtual void initIntro();
    virtual void initOutro();
    
    //Special scene intro/outro functions
    void initIntroWithInitialInstructions();
    
    //Play object name after recording
    void playObjectName();
    
    //Functions for setting up the test
    void setImagePaths(vector<string> imagePaths);
    void setObjectVoicePaths(vector<string> objectVoicePaths);
    void setTestId(vector<int> testIds);
    
    //Recording related functions
    bool isRequestRecord();
    void stopRecording();
    int getCurrentTestId(); //Get current text id for naming recorded audio file
    void setSoundLevel(float soundLevel);
    
    void togglePause();
    
private:
    virtual void intro();
    virtual void outro();
    
    vector<TABInteractiveObject> productionObjects;
    vector<string> productionObjectsVoicePaths;
    vector<int> productionTestId;
    ofRectangle insideCartObject;
    
    vector<TABInteractiveObject> meter;
    
    
    int bgImageId;
    int topImageId;
    TABInteractiveObject registerDisplayOn; 
    
    
    bool bRequestRecord;
    
    string currentState;
    string lastState;
    
    //
    ofPoint objectCartPos;
    ofPoint objectPeakPos;
    ofPoint objectBeltPos;
    ofPoint objectScannerPos;
    ofPoint objectAwayPos;
    
    void initialInstructions();
    void instructions();

    void initObjectCartToBelt();
    void initObjectBeltToScanner();
    void initRecordVoiceSignal();
    void initRecordVoice();
    void initObjectScannerAway();
    
    void objectCartToBelt();
    void objectBeltToScanner();
    void objectScannerAway();
    
    
    void playObject();
    
    
    bool bPause;
    
    //
    void recordVoiceSignal();
    void recordVoice();
    unsigned long long recordTimer;
    string recordVoiceSignalSoundfile;
    
    //
    ofxTween sceneTween;
    ofxEasingLinear easinglinear;
    
    //
    ofxTween objectsTween;
    ofxEasingBounce easingbounce;
    ofPolyline objectTrajectory;
    
    
    
    //
    
    void initInstruction();
    void initInitialInstruction();
    
    //
    ofSoundPlayer soundPlayer;
    float soundLevel;
    string initialInstructionsFilename, testInstructionsFilename;
    
    //
    bool bInitialInstructions;
    
    
};

#endif /* defined(__TABLEApp__TABProductionScene__) */
