//
//  TABEndLevelScene.h
//  TABLEApp
//
//  Created by Leonardo Amico on 16/10/14.
//
//

#ifndef __TABLEApp__TABEndLevelScene__
#define __TABLEApp__TABEndLevelScene__

#define STATE_END_LEVEL_INTRO                               "END_LEVEL - INTRO"
#define STATE_END_LEVEL_ANIMATION                           "END_LEVEL - ANIMATION"

#define STATE_END_LEVEL_OUTRO                               "END_LEVEL - OUTRO"
#define STATE_END_LEVEL_IDLE                                "END_LEVEL - IDLE"


#include "TABBaseScene.h"
#include "TABInteractiveObject.h"
#include "TABFallingObject.h"
#include "ofxTween.h"

class TABEndLevelScene: public TABBaseScene
{
public:
    TABEndLevelScene();
    
    //General scene functions
    virtual void update();
    virtual void draw();
    
    virtual void initIntro();
    virtual void initOutro();

    virtual void touchDown(ofPoint touchPt);
    virtual void touchMoved(ofPoint touchPt);
    virtual void touchUp(ofPoint touchPt);
    
    void setLevel(int idLevel, bool bEndGame = false);
    
private:
    virtual void intro();
    virtual void outro();
    
    void initAnimation();
    void animation();
    
    string currentState;
    
    //
    ofxTween sceneTween;
    ofxEasingLinear easinglinear;
    
    //
    ofxTween objectsTween;
    ofxEasingBounce easingbounce;
    
    int idLevel; 
    bool bEndGame;
    
    ///Users/amicoleo/SpiderOak Hive/__projects/Table_iOSApp/SW/OF_iOS/of_v0.8.4_ios_release/apps/myApps/TABLEApp/src/TABMenuScene.h
    vector<ofPoint> endLevelImagesPos;
    
    //
    ofSoundPlayer soundPlayer;
    
    //
    int medalImgId;
    int bgImageId;
    vector<TABFallingObject> fallingObjects;
    vector<TABInteractiveObject> levelMedals;
    
    TABInteractiveObject continueButton;
    
    bool bSceneExited; 
    
    
};

#endif /* defined(__TABLEApp__TABEndLevelScene__) */
