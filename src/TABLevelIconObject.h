//
//  TABLevelIconObject.h
//  TABLEApp
//
//  Created by Leonardo Amico on 09/10/14.
//
//

#ifndef __TABLEApp__TABLevelIconObject__
#define __TABLEApp__TABLevelIconObject__

#include "TABButtonObject.h"

class TABLevelIconObject: public TABButtonObject {
public:
    TABLevelIconObject(string imgPath, ofPoint centerPos, bool _bActive = false);
    void draw();
    
    bool bActive;
    
};

#endif /* defined(__TABLEApp__TABLevelIconObject__) */
