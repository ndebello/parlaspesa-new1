//
//  TABIntroScene.h
//  ParlaSpesa
//
//  Created by Leonardo Amico on 26/11/14.
//
//

#ifndef __ParlaSpesa__TABIntroScene__
#define __ParlaSpesa__TABIntroScene__

#include "TABBaseScene.h"
#include "TABInteractiveObject.h"
#include "TABAnimatedObject.h"
#include "ofxXmlSettings.h"
#include "ofxTween.h"


#define STATE_INTRO_INTRO                         "INTRO - INTRO"
#define STATE_INTRO_OUTRO                         "INTRO - OUTRO"
#define STATE_INTRO_ANIMATE                       "INTRO - ANIMATE"

class TABIntroScene: public TABBaseScene
{
public:
    TABIntroScene();
    
    //General scene functions
    virtual void update();
    virtual void draw();
    
    virtual void initIntro();
    virtual void initOutro();
    
    void initOutroToMenu();
    
private:
    virtual void intro();
    virtual void outro();
    
    void animate();
    void initAnimate();

    int bgImageId;
    int frontImageId;
    
    string currentState;
    
    ofxTween sceneTween;
    ofxTween objectTween; 
    ofxEasingLinear easinglinear;
    
    TABAnimatedObject cartAnimation;
    
    ofSoundPlayer soundPlayer; 
    string introJingleFilename;
    
    bool bOutroToMenu; 


};

#endif /* defined(__ParlaSpesa__TABIntroScene__) */
