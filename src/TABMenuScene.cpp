//
//  TABMenuScene.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 05/11/14.
//
//

#include "TABMenuScene.h"
#include "TABImagesData.h"

TABMenuScene::TABMenuScene(){
    
    sceneKind = "Menu";
    
    ofxXmlSettings gameXml;
    gameXml.load("GameData.xml");
    gameXml.pushTag("AdditionalSceneSettings");
    gameXml.pushTag("MenuScene");
    
    TABImagesData imagesData;
    bgImageId =  imagesData.getImageId(gameXml.getValue("Bg", ""));
    
    creditsButton = TABButtonObject(gameXml.getValue("CreditsButton", ""), ofPoint(ofGetWidth()*0.18,ofGetHeight()*0.89));
    resultsButton = TABButtonObject(gameXml.getValue("ResultsButton", ""), ofPoint(ofGetWidth()*0.81,ofGetHeight()*0.89));
    startButton = TABButtonObject(gameXml.getValue("StartButton", ""), ofPoint(ofGetWidth()*0.5,ofGetHeight()*0.85));
    
    maleButton = TABToggleButtonObject(gameXml.getValue("MaleButton", ""), ofPoint(ofGetWidth()*0.5937,ofGetHeight()*0.664));
    femaleButton = TABToggleButtonObject(gameXml.getValue("FemaleButton", ""), ofPoint(ofGetWidth()*0.6375,ofGetHeight()*0.664));

    gameXml.popTag(); //pop Menu Scene
    
    creditsButton.loadSoundfile(gameXml.getValue("ButtonSound", ""));
    resultsButton.loadSoundfile(gameXml.getValue("ButtonSound", ""));
    startButton.loadSoundfile(gameXml.getValue("ButtonSound", ""));
    
    gameXml.popTag(); //pop Additional Images
    
    //Carousel -
    int nCarouselObjects = 9;
    string carouselObjectsNames[] = {"biscotto.png","ciliegia.png", "coltello.png", "patata.png", "mela.png", "torta.png", "bicchiere.png", "gelato.png", "pane.png"};
    for (int i=0; i<nCarouselObjects; i++){
        objectsCarousel.push_back(TABInteractiveObject(carouselObjectsNames[i], ofPoint( (-i-1)*ofGetWidth()/4,ofGetHeight()/4)));
    }
    
    selectedButton = "";
    
    
}

void TABMenuScene::intro(){
    if (sceneTween.isCompleted())
    {
        ofBackground(190, 21, 34); //Set bg color to smoothen transition to results
        currentState = STATE_MENU_IDLE;
        ofLog()<<"NEW STATE: "<<currentState;
    }
}
void TABMenuScene::outro(){
    if (sceneTween.isCompleted())
        bDead = true;
    
}
void TABMenuScene::initIntro(){
    currentState = STATE_MENU_INTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
    ofBackground(255);
}

void TABMenuScene::initOutro(){
    bCompleted = false;
    currentState = STATE_MENU_OUTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
    ofBackground(255);
}

void TABMenuScene::initOutroToResults(){
    bCompleted = false;
    currentState = STATE_MENU_OUTRO;
}

void TABMenuScene::update(){
    
    if (currentState == STATE_MENU_INTRO){
        this->intro();
        
    }
    else if (currentState == STATE_MENU_OUTRO){
        this->outro();
    }
    else{
        for (int i=0; i<objectsCarousel.size(); i++){
            objectsCarousel[i].x+=4;
            if (objectsCarousel[i].x > ofGetWidth())
                objectsCarousel[i].x-= ofGetWidth()/4*objectsCarousel.size();
        }
    }
}
void TABMenuScene::draw(){
    
    ofPushMatrix();
    ofPushStyle();
    
    if (currentState == STATE_MENU_INTRO){
        if (bIntroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*sceneTween.update());
        }else{
            float xTranslate = ofGetWidth() - sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
    }else if (currentState == STATE_MENU_OUTRO){
        if (bOutroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*(1 -sceneTween.update()));
        }
    }

//    TABImagesData::images[bgImageId].draw(0.102*ofGetWidth(),0);
    TABImagesData::images[bgImageId].draw(0,0);
    
    if (currentState != STATE_MENU_INTRO){
        for (int i=0; i<objectsCarousel.size(); i++){
            if (currentState == STATE_MENU_OUTRO){
                if (objectsCarousel[i].x + objectsCarousel[i].width< ofGetWidth() && objectsCarousel[i].x > 0)
                    objectsCarousel[i].draw();
            }
            else{
                objectsCarousel[i].draw();
            }
        }
    }
    creditsButton.draw();
    resultsButton.draw();
    startButton.draw();
    maleButton.draw();
    femaleButton.draw();
    ofPopStyle();
    ofPopMatrix();
}

void TABMenuScene::touchDown(ofPoint touchPt){
    
    if (creditsButton.hit(touchPt)){
        
    }
    if (startButton.hit(touchPt)){
        
    }
    if (resultsButton.hit(touchPt)){
        
    }
    if (maleButton.hit(touchPt)){
        
    }
    if (femaleButton.hit(touchPt)){
        
    }
    
}
void TABMenuScene::touchMoved(ofPoint touchPt){
    
}
void TABMenuScene::touchUp(ofPoint touchPt){
    
    if (creditsButton.bDragged){
        creditsButton.released(touchPt);
        if (!creditsButton.bDragged){
            selectedButton = "credits";
            bCompleted = true;
        }
    }
    if (startButton.bDragged){
        startButton.released(touchPt);
        if (!startButton.bDragged){
            selectedButton = "start";
            bCompleted = true;
        }
    }
    if (resultsButton.bDragged){
        resultsButton.released(touchPt);
        if (!resultsButton.bDragged){
            selectedButton = "results";
            bCompleted = true;
        }
    }
    
    if (maleButton.bDragged){
        maleButton.released(touchPt);
        if (!maleButton.bDragged){
            maleButton.setState(true);
            femaleButton.setState(false);
        }
    }
    
    if (femaleButton.bDragged){
        femaleButton.released(touchPt);
        if (!femaleButton.bDragged){
            maleButton.setState(false);
            femaleButton.setState(true);
        }
    }
    
}

string TABMenuScene::getSelectedButton(){
    return selectedButton;
}

void TABMenuScene::resetSelectedButton(){
    selectedButton = ""; 
}

int TABMenuScene::getGender(){
    if (femaleButton.getState())
        return GENDER_FEMALE;
    else if (maleButton.getState())
        return GENDER_MALE;
    else
        return -1; 
}


