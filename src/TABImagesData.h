//
//  TABImagesData.h
//  TABLEApp
//
//  Created by Leonardo Amico on 08/10/14.
//
//

#ifndef __TABLEApp__TABImagesData__
#define __TABLEApp__TABImagesData__

#include "ofMain.h"

class TABImagesData
{
public:
    static bool bIsRetinaEnabled; 
    void loadImages();
    static ofImage bgImage;
    
    static ofImage medalImage;
    static ofImage cupImage;
    
    static vector<ofImage> images;
    static vector<string> imagesFilenames;
    void setupImages();
    int getImageId(string filename, bool bLoadImage = true);
    
    
private:
    string addImageDirPath(string imageFileName);
    
};

#endif /* defined(__TABLEApp__TABImagesData__) */
