//
//  TABComprensionObject.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 09/10/14.
//
//

#include "TABComprensionObject.h"

TABComprensionObject::TABComprensionObject(string imgPath, ofPoint centerPos, bool _bCorrect, bool _bSemanticDistractor):TABInteractiveObject(imgPath, centerPos)
{
    bCorrect = _bCorrect;
    bSemanticDistractor = _bSemanticDistractor; 
}