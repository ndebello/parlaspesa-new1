//
//  TABTestViewController.m
//  TABLEApp
//
//  Created by Leonardo Amico on 21/10/14.
//
//

#import "TABTestViewController.h"
#import "TABSoundTool.h"

@interface TABTestViewController ()

@property TABSoundTool* soundTool;
@property (weak, nonatomic) IBOutlet UILabel *testInfo;
@property (weak, nonatomic) IBOutlet UILabel *testDate;
@property (weak, nonatomic) IBOutlet UILabel *userInfo;

@end

@implementation TABTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.soundTool = [[TABSoundTool alloc] init];
    [self configureView]; 
}

- (void) configureView {
    
    NSString* userInfoText = [NSString stringWithFormat:@"%@, %@ anni", [self.userData name], [self.userData age]];
    [[self userInfo] setText:userInfoText];
    
    NSString* levelId = self.test.levelId;
    NSString* testId =  self.test.testId;
    NSString* date = self.test.time;

    self.testInfo.text = [NSString stringWithFormat:@"Level %@, test %@", levelId, testId];
    
    self.testDate.text = date;
    
    NSMutableString* attemptSequence = [[NSMutableString alloc] init];
    for (int i=0; i< [self.test.attemptsSequence count]; i++){
        NSString* attempt;
        if ([self.test.attemptsSequence[i]  isEqual: @"Correct"]){
            attempt = @"corretto";
        }else if ([self.test.attemptsSequence[i]  isEqual: @"SemanticDistractor"]){
            attempt = @"distrattore semantico";
        }else{
            attempt = @"distrattore";
        }
        if (i > 0)
            [attemptSequence appendString:@", "];
        [attemptSequence appendString:attempt];
    }
    self.resultSequence.text = attemptSequence;
}

- (IBAction)playRecording:(id)sender {
    [self.soundTool play:self.test.productionVoiceRecordingPath isProductionSoundfile:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}

- (void)viewWillDisappear:(BOOL)animated {
    //Reproduce button sound
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"buttonClick"
                                                         ofType:@"mp3"];
    [self.soundTool play:filePath isProductionSoundfile:NO];

}

@end
