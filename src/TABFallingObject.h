//
//  TABFallingObject.h
//  TABLEApp
//
//  Created by Leonardo Amico on 14/11/14.
//
//

#ifndef __TABLEApp__TABFallingObject__
#define __TABLEApp__TABFallingObject__

#include "TABInteractiveObject.h"

class TABFallingObject: public TABInteractiveObject {
public:
    TABFallingObject(string imgPath, ofPoint centerPos);
    void update();
private:
    float speed; 
    
    
    
};

#endif /* defined(__TABLEApp__TABFallingObject__) */
