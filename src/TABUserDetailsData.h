//
//  TABUserDetailsData.h
//  TABLEApp
//
//  Created by Leonardo Amico on 20/10/14.
//
//

#import <Foundation/Foundation.h>
#import "TABTestDetailData.h"

@interface TABUserDetailsData : NSObject


@property NSInteger nLevels; 

@property NSString* name;
@property NSString* age;

@property NSArray* tests; //Array of tests objects
//@property NSString name;


@end
