//
//  TABLevelMenuScene.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 08/10/14.
//
//

#include "TABLevelMenuScene.h"


TABLevelMenuScene::TABLevelMenuScene(int _nLevels) {
    construct(_nLevels, _nLevels-1);
}

TABLevelMenuScene::TABLevelMenuScene(int _nLevels, int _lastLevelAvailable) {
    construct(_nLevels, _lastLevelAvailable);
}

void TABLevelMenuScene::construct(int _nLevels, int _lastLevelAvailable){
    sceneKind = "Level menu";
    currentState = STATE_LEVEL_MENU_IDLE;
    selectedLevel = -1;
    bLevelSelected = false;
    
    nLevels = _nLevels;
    lastLevelAvailable = _lastLevelAvailable;
    
    
    ofxXmlSettings gameXml;
    gameXml.load("GameData.xml");
    gameXml.pushTag("AdditionalSceneSettings");
    gameXml.pushTag("LevelScene");
    
    
    TABImagesData imagesData;
    bgImageId =  imagesData.getImageId(gameXml.getValue("Bg", ""));
    
    gameXml.pushTag("Level1");
    levelIcons.push_back(TABLevelIconObject(gameXml.getValue("Img",""),ofPoint(0,0)));
    levelIcons[levelIcons.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
    gameXml.pushTag("Level2");
    levelIcons.push_back(TABLevelIconObject(gameXml.getValue("Img",""), ofPoint(gameXml.getValue("xRelPos",0)*ofGetWidth(),gameXml.getValue("yRelPos",0)*ofGetHeight())));
    levelIcons[levelIcons.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
    gameXml.pushTag("Level3");
    levelIcons.push_back(TABLevelIconObject(gameXml.getValue("Img",""), ofPoint(gameXml.getValue("xRelPos",0)*ofGetWidth(),gameXml.getValue("yRelPos",0)*ofGetHeight())));
    levelIcons[levelIcons.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
    gameXml.pushTag("Level4");
    levelIcons.push_back(TABLevelIconObject(gameXml.getValue("Img",""), ofPoint(gameXml.getValue("xRelPos",0)*ofGetWidth(),gameXml.getValue("yRelPos",0)*ofGetHeight())));
    levelIcons[levelIcons.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
    gameXml.pushTag("Level5");
    levelIcons.push_back(TABLevelIconObject(gameXml.getValue("Img",""), ofPoint(gameXml.getValue("xRelPos",0)*ofGetWidth(),gameXml.getValue("yRelPos",0)*ofGetHeight())));
    levelIcons[levelIcons.size()-1].setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
    gameXml.popTag(); //pop Level Scene
    
    for (int i=0; i< levelIcons.size(); i++){
        levelIcons[i].loadSoundfile(gameXml.getValue("ButtonSound", ""));
    }
    
    gameXml.popTag(); //pop Additional Images
    
    //Set active levels
    for (int i=0; i<nLevels; i++){
        if (i <= lastLevelAvailable)
            levelIcons[i].bActive = true; 
    }
        
    ofBackground(255);

}
void TABLevelMenuScene::intro(){
    if (sceneTween.isCompleted())
    {
        currentState = STATE_LEVEL_MENU_IDLE;
        ofLog()<<"NEW STATE: "<<currentState;
    }

}
void TABLevelMenuScene::outro(){
    if (sceneTween.isCompleted())
        bDead = true;
    
}
void TABLevelMenuScene::initIntro(){
    currentState = STATE_LEVEL_MENU_INTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABLevelMenuScene::initRestartIntro(){
    currentState = STATE_LEVEL_MENU_RESTART_INTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABLevelMenuScene::initOutro(){
    bCompleted = false; 
    currentState = STATE_LEVEL_MENU_OUTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABLevelMenuScene::update(){
    
    if (currentState == STATE_LEVEL_MENU_INTRO){
        this->intro();
        
    }
    else if (currentState == STATE_LEVEL_MENU_OUTRO){
        this->outro();
    }
}
void TABLevelMenuScene::draw(){
    
    ofPushMatrix();
    ofPushStyle();
    if (currentState == STATE_LEVEL_MENU_INTRO){
        if (bIntroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*sceneTween.update());
        }else{
            float xTranslate = ofGetWidth() - sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
    }else if (currentState == STATE_LEVEL_MENU_OUTRO){
        if (bOutroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*(1 -sceneTween.update()));
        }else{
            float  xTranslate = -sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
    }
    
    TABImagesData::images[bgImageId].draw(0,0);
    for (int i=0; i<levelIcons.size(); i++){
        levelIcons[i].draw();
    }
    ofPopStyle();
    ofPopMatrix();
}

void TABLevelMenuScene::touchDown(ofPoint touchPt){
    if (!bLevelSelected){
        for (int i=0; i<levelIcons.size(); i++)
        {
            if (!levelIcons[i].bActive)
                continue;
            levelIcons[i].hit(touchPt);
        }
    }
}
void TABLevelMenuScene::touchMoved(ofPoint touchPt){}
void TABLevelMenuScene::touchUp(ofPoint touchPt){
    if (!bLevelSelected){
        for (int i=0; i<levelIcons.size(); i++)
        {
            if (!levelIcons[i].bActive)
                continue;

            
            if (levelIcons[i].bDragged){
                levelIcons[i].released(touchPt);
                if (!levelIcons[i].bDragged){
                    selectedLevel = i;
                    bCompleted = true;
                    bLevelSelected = true;
                }
            }
        }
    }
}

int TABLevelMenuScene::getSelectedLevel()
{
    return selectedLevel;
}
