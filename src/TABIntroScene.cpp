//
//  TABIntroScene.cpp
//  ParlaSpesa
//
//  Created by Leonardo Amico on 26/11/14.
//
//

#include "TABIntroScene.h"

TABIntroScene::TABIntroScene(){
    
    sceneKind = "Intro";
    
    ofxXmlSettings gameXml;
    gameXml.load("GameData.xml");
    gameXml.pushTag("AdditionalSceneSettings");
    gameXml.pushTag("IntroScene");
    

    TABImagesData imagesData;
    bgImageId =  imagesData.getImageId(gameXml.getValue("Bg", ""));
    frontImageId = imagesData.getImageId(gameXml.getValue("Front", ""));
    introJingleFilename = gameXml.getValue("Jingle", "");
    
    gameXml.pushTag("Animation");
    for (int i=0; i<gameXml.getNumTags("Frame"); i++){
        cartAnimation.frames.push_back(TABInteractiveObject(gameXml.getValue("Frame", "", i), ofPoint(-400,0.72*ofGetHeight())));
    }
    gameXml.popTag(); //pop animation

    
    gameXml.popTag(); //pop Menu Scene
    gameXml.popTag(); //pop Additional Images

    
    ofBackground(255);
    
    bOutroToMenu = false;

    
}

void TABIntroScene::intro(){
    if (sceneTween.isCompleted()){
        initAnimate();
    }
}

void TABIntroScene::animate(){
    
    for (int i=0; i<cartAnimation.frames.size(); i++)
        cartAnimation.frames[i].x = objectTween.update();
    
//    cartAnimation.update();
    
    if (objectTween.isCompleted())
        bCompleted = true;
}

void TABIntroScene::outro(){
    if (sceneTween.isCompleted())
        bDead = true;
    
    if (bOutroToMenu)
        soundPlayer.setVolume(1-sceneTween.update()); 
}
void TABIntroScene::initIntro(){
    currentState = STATE_INTRO_INTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}


void TABIntroScene::initOutro(){
    bCompleted = false;
    currentState = STATE_INTRO_OUTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABIntroScene::initOutroToMenu(){
    bOutroToMenu = true;
    initOutro();
}

void TABIntroScene::initAnimate(){
    soundPlayer.loadSound(introJingleFilename);
    soundPlayer.play();
    currentState = STATE_INTRO_ANIMATE;
    objectTween.setParameters(0, easinglinear, ofxTween::easeOut, -600, ofGetWidth() + 10, INTRO_ANIMATION_DURATION, 0);
}


void TABIntroScene::update(){
    
    if (currentState == STATE_INTRO_INTRO){
        this->intro();
        
    }
    else if (currentState == STATE_INTRO_OUTRO){
        this->outro();
    }
    else{
        this->animate(); 
    }
}
void TABIntroScene::draw(){
    
    ofPushMatrix();
    ofPushStyle();
    if (currentState == STATE_INTRO_INTRO){
        if (bIntroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*sceneTween.update());
        }else{
            float xTranslate = ofGetWidth() - sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
    }else if (currentState == STATE_INTRO_OUTRO){
        if (bOutroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*(1 -sceneTween.update()));
        }else{
            float  xTranslate = -sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
    }

    
    TABImagesData::images[bgImageId].draw(0,0);
    if (currentState == STATE_INTRO_ANIMATE){
        cartAnimation.update();
        cartAnimation.draw();
    }
    TABImagesData::images[frontImageId].draw(ofGetWidth() - TABImagesData::images[frontImageId].getWidth(),0);
    ofPopStyle();
    ofPopMatrix();
    
}



