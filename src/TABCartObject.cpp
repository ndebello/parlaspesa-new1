//
//  TABCartObject.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 23/10/14.
//
//

#include "TABCartObject.h"

TABCartObject::TABCartObject():TABInteractiveObject() {
}

TABCartObject::TABCartObject(string imgPath, ofPoint centerPos):TABInteractiveObject(imgPath, centerPos) {
    bDebug = false;
    detectionMargin = 40;
    insideRect = ofRectangle(x,y,width,height);
    activeRect = ofRectangle(x,y,width,height);
}

void TABCartObject::setActiveRectMargins(int topMargin, int bottomMargin, int leftMargin, int rightMargin){
    activeRect.y = y + topMargin;
    activeRect.x = x + leftMargin;
    activeRect.height = height - topMargin - bottomMargin;
    activeRect.width = width - leftMargin - rightMargin;
}

void TABCartObject::setInsideRectMargins(int topMargin, int bottomMargin, int leftMargin, int rightMargin){
    insideRect.y = y + topMargin;
    insideRect.x = x + leftMargin;
    insideRect.height = height - topMargin - bottomMargin;
    insideRect.width = width - leftMargin - rightMargin;
}

void TABCartObject::setDebugMode(bool _bDebug){
    bDebug = _bDebug;
}

void TABCartObject::draw(){
    TABInteractiveObject::draw();
    
    if (bDebug){
        ofPushStyle();
        ofSetColor(ofColor::red);
        ofNoFill();
        ofSetLineWidth(3);
        ofBeginShape();
        ofVertex(insideRect.x, insideRect.y);
        ofVertex(insideRect.x, insideRect.y + insideRect.height);
        ofVertex(insideRect.x + insideRect.width, insideRect.y + insideRect.height);
        ofVertex(insideRect.x + insideRect.width, insideRect.y);
        ofEndShape();
        ofCircle(insideRect.getCenter().x, insideRect.getCenter().y, 20);
        
        ofSetColor(ofColor::blue, 40);
        ofFill(); 
        ofBeginShape();
        ofVertex(activeRect.x, activeRect.y);
        ofVertex(activeRect.x, activeRect.y + activeRect.height);
        ofVertex(activeRect.x + activeRect.width, activeRect.y + activeRect.height);
        ofVertex(activeRect.x + activeRect.width, activeRect.y);
        ofEndShape();
        
        ofPopStyle();
        
    }
    
}

bool TABCartObject::intersectBorder(const ofRectangle &collidingObject){
    

    //right
    for (int yInc = 0; yInc < insideRect.height; yInc++){
        if (collidingObject.inside(insideRect.x + insideRect.width, insideRect.y + yInc))
            return true;
    }
    
    //bottom
    for (int xInc = 0; xInc < insideRect.width; xInc++){
        if (collidingObject.inside(insideRect.x + xInc, insideRect.y + insideRect.height))
            return true;
    }
    //left
    for (int yInc = 0; yInc < insideRect.height; yInc++){
        if (collidingObject.inside(insideRect.x , insideRect.y + yInc))
            return true;
    }
    
    return false;
}
bool TABCartObject::isInsideActiveRect(ofPoint pt){
    return activeRect.inside(pt);
}

ofPoint TABCartObject::getInsideRectCenter(){
    return insideRect.getCenter(); 
}

ofRectangle TABCartObject::getInsideRect(){
    return insideRect; 
}