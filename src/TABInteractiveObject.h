//
//  TABInteractiveObject.h
//  TABLEApp
//
//  Created by Leonardo Amico on 23/09/14.
//
//

#ifndef __TABLEApp__TABInteractiveObject__
#define __TABLEApp__TABInteractiveObject__

#include "ofMain.h"
#include "TABImagesData.h"



class TABInteractiveObject: public ofRectangle
{
    
public:
    
    TABInteractiveObject();
    TABInteractiveObject(string imgPath, ofPoint centerPos);
    void setImg(string imgPath, ofPoint centerPos);
    void draw();
    
    bool bDragged;
    bool hit(ofPoint touchPt);
    void released(ofPoint touchPt);
    void dragged(ofPoint touchPt);
    
        
    int imageId; 
    ofPoint touchAnchorOffset; //TODO - change name in touchAnchorOffsetOffset, this one is misleading
    ofPoint originalPos;

protected:
    bool bImgLoaded;
    string imgPath; 
    
    
    
};
#endif /* defined(__TABLEApp__TABInteractiveObject__) */
