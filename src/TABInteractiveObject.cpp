//
//  TABInteractiveObject.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 23/09/14.
//
//

#include "TABInteractiveObject.h"

TABInteractiveObject::TABInteractiveObject()
{
    bDragged = false;
    imgPath = "";
}

TABInteractiveObject::TABInteractiveObject(string _imgPath, ofPoint centerPos)
{
    setImg(_imgPath,centerPos);
    originalPos = position; 
    
}


void TABInteractiveObject::setImg(string _imgPath, ofPoint centerPos)
{
    imgPath = _imgPath;
    
    bDragged = false;
    TABImagesData imagesData;
    imageId =  imagesData.getImageId(imgPath);
    bImgLoaded = false;
    if (imageId != -1)
        bImgLoaded = true;
    
    if (bImgLoaded) {
        width = TABImagesData::images[imageId].getWidth();
        height = TABImagesData::images[imageId].getHeight();
    }
    else {
        width = 250;
        height = 250;
    }
    position.x = centerPos.x - width*.5;
    position.y = centerPos.y - height*.5;
    
}

bool TABInteractiveObject::hit(ofPoint touchPt)
{
    if (inside(touchPt)) {
        bDragged = true;
        touchAnchorOffset.x = touchPt.x - x;
        touchAnchorOffset.y = touchPt.y - y;
    }
    return bDragged;
}
void TABInteractiveObject::released(ofPoint touchPt)
{
    if (inside(touchPt))
        bDragged = false;
}
void TABInteractiveObject::dragged(ofPoint touchPt)
{
    if (bDragged) {
        x = touchPt.x - touchAnchorOffset.x;
        y = touchPt.y - touchAnchorOffset.y;
    }
}

void TABInteractiveObject::draw()
{
    if (bImgLoaded)
        TABImagesData::images[imageId].draw(x, y, width, height);
    else {
        ofPushStyle();
        ofSetColor(ofColor::yellowGreen);
        ofRect(*this);
        ofSetColor(255);
        ofPoint stringPos = position + ofPoint(10,10);
        ofDrawBitmapString(imgPath, stringPos); 
        ofPopStyle();
    }
}

