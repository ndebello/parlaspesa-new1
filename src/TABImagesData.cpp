//
//  TABImagesData.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 08/10/14.
//
//

#include "TABImagesData.h"

//ofImage TABImagesData::bgImage;
//
//ofImage TABImagesData::medalImage;
//ofImage TABImagesData::cupImage;

vector<ofImage> TABImagesData::images;
vector<string> TABImagesData::imagesFilenames;
bool TABImagesData::bIsRetinaEnabled;


void TABImagesData::loadImages()
{
    static bool bImagesLoaded = false;
    if (!bImagesLoaded){
        bImagesLoaded = true;
        setupImages();
    }
}


void TABImagesData::setupImages(){
    
    ofDirectory dir;
    dir.listDir("Images/");
    dir.allowExt("png");
    dir.allowExt("jpg");
    dir.listDir(); //TODO - delete this?
    
    //If retina - load just images with @2x suffix
    if (bIsRetinaEnabled){
        cout<<"Hi retina"<<endl; //TODO - test
        for (int i=0; i<dir.numFiles(); i++){
            size_t found = dir.getName(i).find("@2x");
            if (found!=string::npos)
                imagesFilenames.push_back(dir.getName(i));
        }
    }else{
        for (int i=0; i<dir.numFiles(); i++){
            size_t found = dir.getName(i).find("@2x");
            if (found==string::npos)
                imagesFilenames.push_back(dir.getName(i));
        }
    }
    images.resize(imagesFilenames.size());
}

int TABImagesData::getImageId(string _filename, bool bLoadImage){
    string filename = _filename;
    
    //If retina enabled
    if (bIsRetinaEnabled){
        if (filename.find(".png") != string::npos)
            filename.replace(filename.find(".png"), filename.length(), "@2x.png");
        if (filename.find(".jpg") != string::npos)
            filename.replace(filename.find(".jpg"), filename.length(), "@2x.jpg");
    }
    for (int i=0; i<imagesFilenames.size(); i++){
        if (imagesFilenames[i] == filename){
            if (bLoadImage){
                if (images[i].isAllocated()){
                    return i;
                }else{
                    if (images[i].loadImage(addImageDirPath(filename))){
                        return i;
                    }
                }
            }else{
                return i; 
            }
        }
    }
    return -1;
}


string TABImagesData::addImageDirPath(string imageFileName)
{
    stringstream ss;
    ss<<"Images/"<<imageFileName;
    return ss.str();
    
}

