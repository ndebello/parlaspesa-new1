//
//  TABUsersViewController.m
//  TABLEApp
//
//  Created by Leonardo Amico on 28/10/14.
//
//

#import <JXLS/JXLS.h>

#import "TABUsersViewController.h"
//#import "TABUserTableViewController.h"
#import "TABUserViewController.h"
#import "TABSoundTool.h"


#import <DropboxSDK/DropboxSDK.h>

#import "GDataXMLNode.h"

#import "Reachability.h"


@interface TABUsersViewController ()  <DBRestClientDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray* users;

@property (retain, nonatomic) IBOutlet UIBarButtonItem *linkDBButton;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *syncDBButton;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *syncToDropboxIndicator;

@property TABSoundTool* soundTool;

@property (nonatomic, strong) DBRestClient *restClient;

@property NSMutableArray* existingFilesAtDB;
@property int nFilesToUpload;

@end


@implementation TABUsersViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateDBInteraction];
    [self loadXML];
    
    //
    self.nFilesToUpload = 0;
    
    // Setup sync to db activity indicator
    [self.syncToDropboxIndicator setHidesWhenStopped:YES];
    [self.syncToDropboxIndicator stopAnimating];
    
    self.soundTool = [[TABSoundTool alloc] init];

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Delete from xml
    NSString* name = [self.users[indexPath.row] name];
    NSString* age = [self.users[indexPath.row] age];
    [self deleteUser:name :age];
    
    // Remove the row from data model
    [self.users removeObjectAtIndex:indexPath.row];
    
    //Reload data
    [self.tableView reloadData];
    
    //
    JXLSWorkBook *workBook = [JXLSWorkBook new];
    JXLSWorkSheet *workSheet = [workBook workSheetWithName:@"SHEET1"];
}


//These methods are for unpdating the "connect/disconnect to dropbox button when user changes active app (as when going to the "
- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDBInteraction) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Reproduce button sound
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"buttonClick"
                                                         ofType:@"mp3"];
    [self.soundTool play:filePath isProductionSoundfile:NO];    
    
}

- (IBAction)unwindToResults:(UIStoryboardSegue *)segue{
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.users count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell" forIndexPath:indexPath];
    
    NSString* name = [self.users[indexPath.row] name];
    NSString* age = [self.users[indexPath.row] age];
    
    NSArray *cellSubviews = [cell.contentView subviews];
    for (UIView* subview in cellSubviews){
        
        if ([subview tag] == 0){
            UILabel* nameLabel = (UILabel*)subview;
            [nameLabel setText:name];
        }else if ([subview tag] == 1){
            UILabel* ageLabel = (UILabel*)subview;
            [ageLabel setText:age];
        }
    }
//    (UILabel*)[cellSubviews objectAtIndex:0];
//    [cellSubviews objectAtIndex:1];
//    if ([cellSubviews count] != 0){
//        
//    }
//    
//    UILabel* nameLabel = (UILabel*)[cell viewWithTag:0];
//    UILabel* ageLabel = (UILabel*)[cell viewWithTag:1];
////    [nameLabel setText:name];
//    [nameLabel setText:@"ciao"];
////    [ageLabel setText:age]; 
    
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showUserDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        TABUserDetailsData* user = self.users[indexPath.row];
        
        //This crashes on iOS7
//        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
//        TABUserTableViewController* userTableViewController = (TABUserTableViewController*)[navController topViewController];
//        [userTableViewController setUserData:user];
//        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        //TEST - this is good on iOS7 - but could crash in iOS8
//        NSLog(@"Nome: %@ - età: %@", user.name, user.age);
//        TABUserTableViewController* userTableViewController = (TABUserTableViewController*)[segue destinationViewController];
//        [userTableViewController setUserData:user];
//        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        //No more tableViewController - iOS7
        TABUserViewController* userViewController = (TABUserViewController*)[segue destinationViewController];
        [userViewController setUserData:user];
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        
    }
}

#pragma mark - xml methods

- (void) deleteUser:(NSString*)name :(NSString*)age{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[self getUserResultFilename]];
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];
    NSError *error;
    
    GDataXMLDocument *xmlDoc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    if (xmlDoc == nil)
    {
        NSLog(@"XML not found");
        return;
    }
    
    NSString* xPath = [NSString stringWithFormat:@"//User[@name='%@' and @age='%@']",name, age];
    NSArray* xmlUserToRemoveArray = [xmlDoc nodesForXPath:xPath error:nil];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //Remove all nodes with specified name & age (it can't be more then one - because if login data already exists, no new user entry is created
    for (GDataXMLElement* xmlUserToRemove in xmlUserToRemoveArray){
        NSLog(@"%@", xmlUserToRemove);
        NSArray* xmlLevels = [xmlUserToRemove elementsForName:@"Level"];
        if ([xmlLevels count] > 0){
            for (GDataXMLElement* xmlLevel in xmlLevels){
                NSArray* xmlTests = [xmlLevel elementsForName:@"Test"];
                if ([xmlTests count] > 0){
                    for (GDataXMLElement* xmlTest in xmlTests){
                        NSArray* xmlVoiceRecording = [xmlTest elementsForName:@"VoiceRecording"];
                        if ([xmlVoiceRecording count] > 0){
                            NSError* error;
                            [fileManager removeItemAtPath:[self getSoundFilePath:[[xmlVoiceRecording objectAtIndex:0] stringValue]] error:&error];
                            if (error){
                                NSLog(@"Could not delete soundfile at: %@",[self getSoundFilePath:[[xmlVoiceRecording objectAtIndex:0] stringValue]]);
                            }else {
//                                NSLog(@"Deleted soundfile at: %@",[self getSoundFilePath:[[xmlVoiceRecording objectAtIndex:0] stringValue]]);
                            }
                        }
                    }
                }
            }
        }
        [[xmlDoc rootElement] removeChild:xmlUserToRemove];
    }
    
    
    
    //Save back to the file
    xmlData = xmlDoc.XMLData;
    [xmlData writeToFile:filePath atomically:YES];
}

- (void) loadXML{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[self getUserResultFilename]];
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:filePath];
    NSError *error;
    
    GDataXMLDocument *xmlDoc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    if (xmlDoc == nil)
    {
        NSLog(@"XML not found");
        return;
    }
        
    NSMutableArray* users = [[NSMutableArray alloc] init];
    
    NSArray *xmlUsers = [xmlDoc.rootElement elementsForName:@"User"];
    for (GDataXMLElement* xmlUser in xmlUsers)
    {
        TABUserDetailsData* user = [[TABUserDetailsData alloc] init];
        user.name = [[xmlUser attributeForName:@"name"] stringValue];
        user.age  = [[xmlUser attributeForName:@"age"] stringValue];
        
        NSMutableArray* tests = [[NSMutableArray alloc] init];
        NSArray* xmlLevels = [xmlUser elementsForName:@"Level"];
        
        user.nLevels = [xmlLevels count];
        
        if ([xmlLevels count] > 0){
            for (GDataXMLElement* xmlLevel in xmlLevels){
                int levelIdInt = [[[xmlLevel attributeForName:@"id"] stringValue] intValue] + 1; //
                NSString* levelId = [NSString stringWithFormat:@"%d",levelIdInt];
                
                NSArray* xmlTimes = [xmlLevel elementsForName:@"Time"];
                NSString* testTime;
                if ([xmlTimes count] > 0){
                    testTime = [NSString stringWithString:[[xmlTimes objectAtIndex:0] stringValue]];
                }
                
                NSArray* xmlTests = [xmlLevel elementsForName:@"Test"];
                if ([xmlTests count] > 0){
                    for (GDataXMLElement* xmlTest in xmlTests){
                        int testIdInt = [[[xmlTest attributeForName:@"id"] stringValue] intValue] + 1;
                        NSString* testId = [NSString stringWithFormat:@"%d",testIdInt];
                        
                        TABTestDetailData* test = [[TABTestDetailData alloc] init];
                        
                        //Set TABTestDetailData attributes
                        test.levelId = levelId;
                        test.testId = testId;
                        test.time = testTime;
                        
                        NSArray* xmlObjects = [xmlTest elementsForName:@"Objects"];
                        if ([xmlObjects count] > 0){
                            
                            if ([[[xmlObjects objectAtIndex:0] elementsForName:@"Correct"] count] > 0)
                                test.correctObject = [[[[xmlObjects objectAtIndex:0] elementsForName:@"Correct"] objectAtIndex:0] stringValue];
                            if ([[[xmlObjects objectAtIndex:0] elementsForName:@"SemanticDistractor"] count] > 0)
                                test.semanticDistractorObject = [[[[xmlObjects objectAtIndex:0] elementsForName:@"SemanticDistractor"] objectAtIndex:0] stringValue];
                            if ([[[xmlObjects objectAtIndex:0] elementsForName:@"Distractor"] count] > 0){
                                test.distractorAObject = [[[[xmlObjects objectAtIndex:0] elementsForName:@"Distractor"] objectAtIndex:0] stringValue];
                                test.distractorBObject = [[[[xmlObjects objectAtIndex:0] elementsForName:@"Distractor"] objectAtIndex:1] stringValue];
                            }
                        }else{
                            test.result = @"Error - missing value";
                        }
                        
                        NSArray* xmlResults = [xmlTest elementsForName:@"Result"];
                        if ([xmlResults count] > 0){
                            test.result = [[xmlResults objectAtIndex:0] stringValue];
                        }else{
                            test.result = @"Error - missing value"; 
                        }
                        
                        NSArray* xmlAttempts = [xmlTest elementsForName:@"Attempt"];
                        NSMutableArray* attemptsSequence = [[NSMutableArray alloc] init];
                        if ([xmlAttempts count] > 0){
                            for (GDataXMLElement* xmlAttempt in xmlAttempts){
                                [attemptsSequence addObject:[xmlAttempt stringValue]];
                            }
                            test.attemptsSequence = attemptsSequence;
                        }else{
                            [attemptsSequence addObject:@"Error - missing value"];
                            test.attemptsSequence = attemptsSequence;
                        }
                        
                        NSArray* xmlVoiceRecording = [xmlTest elementsForName:@"VoiceRecording"];
                        if ([xmlVoiceRecording count] > 0){
                            test.productionVoiceRecordingPath = [[xmlVoiceRecording objectAtIndex:0] stringValue];
                        }else{
                            test.productionVoiceRecordingPath = @"Error - missing value";
                        }
                        
                        //TEST - TODO - delete
//                        NSLog(@"%@", xmlTest);
                        
                        //
                        [tests addObject:test];
                        
                    }
                }
            }
        }
        user.tests = tests;
        [users addObject:user];
    }
    self.users = users;
}


- (NSString*)getUserResultFilename{
    return [[[[UIDevice currentDevice] identifierForVendor] UUIDString] stringByAppendingString:@"_UserResultsData.xml"];
}

- (NSString* )getSoundFilePath:(NSString*)productionSoundfileName{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:productionSoundfileName];
}


#pragma mark - dropbox
- (IBAction)linkDB:(UIBarButtonItem *)sender {
    if ([self isInternetConnectionAvailable]){
        if (![[DBSession sharedSession] isLinked]) {
            [[DBSession sharedSession] linkFromController:self];
        } else {
            [[DBSession sharedSession] unlinkAll];
            [[[UIAlertView alloc]
               initWithTitle:@"Account Dropbox Disconnesso!" message:@"Hai disconnesso il tuo account Dropbox"
               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
             show];
            [self updateLinkDBButton];
        }
    }else{
        //Alert that there's no internet connection
        [[[UIAlertView alloc]
          initWithTitle:@"Non connesso a internet" message:@"Connettiti a una rete Wi-Fi o riprova piu tardi"
          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
         show];
    }

}

- (void) updateDBInteraction{
    [self updateLinkDBButton];
    if ([[DBSession sharedSession] isLinked]){
        if (self.restClient == nil){
            self.restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
            self.restClient.delegate = self;
        }
    }
}

- (void) updateLinkDBButton{
    NSString* title = [[DBSession sharedSession] isLinked] ? @"Disconnetti Dropbox" : @"Connetti Dropbox";
    [[self linkDBButton] setTitle:title];
}


- (IBAction)syncToDropbox:(id)sender {
    //Check internet connection
    if ([self isInternetConnectionAvailable]){
        if (![[DBSession sharedSession] isLinked]){
            [[[UIAlertView alloc]
               initWithTitle:@"Account Dropbox Disconnesso!" message:@"Prima di sincronizzare i file, connetti Dropbox"
               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
             show];
        }
        else if (self.restClient == nil){
            NSLog(@"There's no DB rest client created");
        }
        else {
            //Get files already the in DB folder and upload the ones which are not there
            [self.restClient loadMetadata:@"/"];
            
            //
            [self buttonsShowDropboxSync:YES];
        }
    }else{
        //Alert that there's no internet connection
        [[[UIAlertView alloc]
          initWithTitle:@"Non connesso a internet" message:@"Connettiti a una rete Wi-Fi o riprova piu tardi"
          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
         show];
        
        [self buttonsShowDropboxSync:NO];
    }
}

- (void)restClient:(DBRestClient *)client uploadedFile:(NSString *)destPath
              from:(NSString *)srcPath metadata:(DBMetadata *)metadata {
    //TODO - test, delete
//    NSLog(@"File uploaded successfully to path: %@", metadata.path);
    self.nFilesToUpload--;
    if (self.nFilesToUpload == 0){
        [self buttonsShowDropboxSync:NO];
        NSLog(@"Sync to Dropbox complete");
    }
    
}

- (void)restClient:(DBRestClient *)client uploadFileFailedWithError:(NSError *)error {
    NSLog(@"File upload failed with error: %@", error);
    NSLog(@"Error loading metadata: %@", error);
    [[[UIAlertView alloc]
      initWithTitle:@"Impossibile caricare file in Dropbox" message:@"Ci sono dei problemi a caricare i file nella cartella Dropbox dell'applicazione. Controlla la connessione internet e riprova."
      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
     show];
    
    [self buttonsShowDropboxSync:NO];
}

- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata {
    if (metadata.isDirectory) {
        //TODO - test, delete
//        NSLog(@"Folder '%@' contains:", metadata.path);
        if ([self existingFilesAtDB] == nil){
            self.existingFilesAtDB = [[NSMutableArray alloc]init];
        } else {
            [self.existingFilesAtDB removeAllObjects];
        }
        
        for (DBMetadata *file in metadata.contents) {
            [[self existingFilesAtDB] addObject:file.filename];
//            NSLog(@"	%@", file.filename);
        }
    }
    [self uploadFilesToDB];
    
}

- (void)restClient:(DBRestClient *)client loadMetadataFailedWithError:(NSError *)error {
    NSLog(@"Error loading metadata: %@", error);
    [[[UIAlertView alloc]
      initWithTitle:@"Impossibile connettersi a Dropbox" message:@"Ci sono dei problemi ad accedere alla cartella Dropbox dell'applicazione. Controlla la connessione internet e riprova."
      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
     show];
    
    [self buttonsShowDropboxSync:NO];
    

}

- (void)uploadFilesToDB{
    
    //Convert xml to xls
    int xmlToXlsRetVal = [self xmlToXls];
    
    //Upload new files
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *destDir = @"/";
    int count;
    NSMutableArray* filesToUploadToDB = [[NSMutableArray alloc] init];
    
    //Retrieve and upload sound files
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    for (count = 0; count < (int)[directoryContent count]; count++)
    {
        NSString* fileName = [directoryContent objectAtIndex:count];
        
        if (![fileName isEqual:[self  getUserResultFilename]]){ //If xml file don't upload - we'll upload the xls instead
            if (![self.existingFilesAtDB containsObject:fileName]){//Upload just if the object is not already there
                [filesToUploadToDB addObject:fileName];
            }
        }
    }
    [self.syncToDropboxIndicator startAnimating];
    for (NSString* fileName in filesToUploadToDB){
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
        [self.restClient uploadFile:fileName toPath:destDir withParentRev:nil fromPath:filePath];//ParentRev: nil prevents data to be overwritten
    }
    
    //Upload user result xls file - if already present delete copy
    if ([self.existingFilesAtDB containsObject:[self getUserResultXLSFilename]]){
        [self.restClient deletePath:[destDir stringByAppendingString:[self getUserResultXLSFilename]]];
        
    }
    [self.restClient uploadFile:[self getUserResultXLSFilename] toPath:destDir withParentRev:nil fromPath:[self getUserResultXLSFilepath]];//ParentRev: nil prevents data to be overwritten
    
    self.nFilesToUpload = [filesToUploadToDB count]+1;//all the soundfiles and the xls file

}

#pragma mark check internet connection for dropbox
// Checks if we have an internet connection or not
- (BOOL)isInternetConnectionAvailable
{
    Reachability* internetReachable = [Reachability reachabilityWithHostname:@"api.dropbox.com"];
    return internetReachable.isReachable;
}

- (void)buttonsShowDropboxSync:(BOOL)syncToDropbox{
    if (syncToDropbox){
        [self.syncDBButton setTitle:@"Caricando dati su Dropbox..."];
        [self.syncDBButton setEnabled:NO];
        [self.linkDBButton setEnabled:NO];
        [self.syncToDropboxIndicator startAnimating];
    }else{
        //Restore original view
        [self.syncDBButton setTitle:@"Sincronizza in Dropbox"];
        [self.syncDBButton setEnabled:YES];
        [self.linkDBButton setEnabled:YES];
        [self.syncToDropboxIndicator stopAnimating];
    }
}

#pragma mark - xls
- (NSString*)getUserResultXLSFilepath{
    return [NSTemporaryDirectory() stringByAppendingPathComponent:[self getUserResultXLSFilename]];
}

- (NSString*)getUserResultXLSFilename{
    return [[[[UIDevice currentDevice] identifierForVendor] UUIDString] stringByAppendingString:@"_UserResultsData.xls"];
}


- (int)xmlToXls{
    JXLSWorkBook *workBook = [JXLSWorkBook new];
    JXLSWorkSheet *workSheet = [workBook workSheetWithName:@"SHEET1"];
    
    int wNameCell = 50000;
    int wDateCell = 20000;
    int wObjectCell = 20000;
    
    [workSheet setWidth:wNameCell forColumn:0 defaultFormat:NULL];
    [workSheet setWidth:wDateCell forColumn:3 defaultFormat:NULL];
    
    [workSheet setWidth:wObjectCell forColumn:5 defaultFormat:NULL];
    [workSheet setWidth:wObjectCell forColumn:6 defaultFormat:NULL];
    [workSheet setWidth:wObjectCell forColumn:7 defaultFormat:NULL];
    [workSheet setWidth:wObjectCell forColumn:8 defaultFormat:NULL];
    [workSheet setWidth:wObjectCell forColumn:9 defaultFormat:NULL];
    [workSheet setWidth:wObjectCell forColumn:10 defaultFormat:NULL];
    
    [workSheet setWidth:wObjectCell forColumn:11 defaultFormat:NULL];
    
    
    //Titles
    NSMutableArray* titleCells = [[NSMutableArray alloc] init];
    [titleCells addObject:[workSheet setCellAtRow:0 column:0 toString:@"Parlaspesa - id iPad"]];
    [workSheet mergeCellsInRect:(CGRect){{0, 0}, {11, 0}}];
    
    [titleCells addObject:[workSheet setCellAtRow:1 column:0 toString:@"Nome"]];
    [titleCells addObject:[workSheet setCellAtRow:1 column:1 toString:@"Età"]];
    [titleCells addObject:[workSheet setCellAtRow:1 column:2 toString:@"n. Livello"]];
    [titleCells addObject:[workSheet setCellAtRow:1 column:3 toString:@"Data"]];
    [titleCells addObject:[workSheet setCellAtRow:1 column:4 toString:@"n. Test"]];
    
    [titleCells addObject:[workSheet setCellAtRow:1 column:5 toString:@"Oggetti"]];
    [workSheet mergeCellsInRect:(CGRect){{5, 1}, {3, 0}}];
    [titleCells addObject:[workSheet setCellAtRow:2 column:5 toString:@"Corretto"]];
    [titleCells addObject:[workSheet setCellAtRow:2 column:6 toString:@"Distrattore semantico"]];
    [titleCells addObject:[workSheet setCellAtRow:2 column:7 toString:@"Distrattore A"]];
    [titleCells addObject:[workSheet setCellAtRow:2 column:8 toString:@"Distrattore B"]];
    
    [titleCells addObject:[workSheet setCellAtRow:1 column:9 toString:@"Sequenza Risultati"]];
    [workSheet mergeCellsInRect:(CGRect){{9, 1}, {1, 0}}];
    [titleCells addObject:[workSheet setCellAtRow:2 column:9 toString:@"1° Risultato"]];
    [titleCells addObject:[workSheet setCellAtRow:2 column:10 toString:@"2° Risultato"]];
    
    [titleCells addObject:[workSheet setCellAtRow:1 column:11 toString:@"File registrazione produzione"]];
    
    for (JXLSCell *cell in titleCells){
        [cell setFontBold:BOLDNESS_BOLD];
        [cell setHorizontalAlignment:HALIGN_CENTER];
    }
    
    //Create xls file
    NSMutableArray* nameCells = [[NSMutableArray alloc] init];
    NSMutableArray* ageCells = [[NSMutableArray alloc] init];
    NSMutableArray* levelCells = [[NSMutableArray alloc] init];
    NSMutableArray* timeCells = [[NSMutableArray alloc] init];
    NSMutableArray* testCells = [[NSMutableArray alloc] init];
    NSMutableArray* correctCells = [[NSMutableArray alloc] init];
    NSMutableArray* semDisCells = [[NSMutableArray alloc] init];
    NSMutableArray* disCells = [[NSMutableArray alloc] init];
    NSMutableArray* attemptsCells = [[NSMutableArray alloc] init];
    NSMutableArray* productionRecordingCells = [[NSMutableArray alloc] init];

    uint32_t cellRow = 2;
    uint32_t cellCol = 0;
    for (TABUserDetailsData* user in self.users){
        for (TABTestDetailData* test in user.tests){
            [nameCells addObject:[workSheet setCellAtRow:cellRow column:cellCol toString:user.name]];
            [ageCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:user.age]];
            [levelCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:test.levelId]];
            [timeCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:test.time]];
            [testCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:test.testId]];
            [correctCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:test.correctObject]];
            [semDisCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:test.semanticDistractorObject]];
            [disCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:test.distractorAObject]];
            [disCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:test.distractorBObject]];

            [attemptsCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:[test.attemptsSequence objectAtIndex:0]]];
            
            if ([test.attemptsSequence count] > 1)
                [attemptsCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:[test.attemptsSequence objectAtIndex:1]]];
            else
                [attemptsCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:@" -- "]];
            
            [productionRecordingCells addObject:[workSheet setCellAtRow:cellRow column:++cellCol toString:test.productionVoiceRecordingPath]];
            cellRow++;
            cellCol = 0;
        }
    }




    
    //
    
    //Write to file
    return [workBook writeToFile:[self getUserResultXLSFilepath]];
}




@end
