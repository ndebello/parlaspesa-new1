//
//  TABTestViewController.h
//  TABLEApp
//
//  Created by Leonardo Amico on 21/10/14.
//
//

#import <UIKit/UIKit.h>
#import "TABTestDetailData.h"
#import "TABUserDetailsData.h"

@interface TABTestViewController : UIViewController
@property (retain, nonatomic) IBOutlet UILabel *resultSequence;
@property (retain, nonatomic) IBOutlet UINavigationItem *navigationBarTitle;

@property TABTestDetailData* test;
@property TABUserDetailsData* userData;


@end
