//
//  TABAnimatedObject.cpp
//  ParlaSpesa
//
//  Created by Leonardo Amico on 27/11/14.
//
//

#include "TABAnimatedObject.h"

TABAnimatedObject::TABAnimatedObject(){
    nUpdatesPerFrame = 8;
    currentAnimationFrame = 0;
    currentFrame = 0;
    bAnimateForward = true;
}
void TABAnimatedObject::draw(){
    frames[currentAnimationFrame].draw();
}
void TABAnimatedObject::update(){
    currentFrame++;
    if (currentFrame%nUpdatesPerFrame == 0){
        currentAnimationFrame+=bAnimateForward?1:-1;
        if (currentAnimationFrame == frames.size()-1 || currentAnimationFrame == 0)
            bAnimateForward=!bAnimateForward;        
    }
}


int TABAnimatedObject::getCurrentAnimationFrame(){
    return currentAnimationFrame;
}
