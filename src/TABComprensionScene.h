//
//  TABComprensionScene.h
//  TABLEApp
//
//  Created by Leonardo Amico on 22/09/14.
//
//

#ifndef __TABLEApp__TABComprensionScene__
#define __TABLEApp__TABComprensionScene__

#include "TABBaseScene.h"
#include "TABComprensionObject.h"
#include "TABAnimatedObject.h"
#include "TABCartObject.h"
#include "ofxTween.h"
#include "ofxXmlSettings.h"

#define STATE_COMPRENSION_INTRO                         "COMPRENSION - INTRO"
#define STATE_COMPRENSION_INITIAL_INSTRUCTIONS          "COMPRENSION - INITIAL_INSTRUCTIONS"
#define STATE_COMPRENSION_WAIT_FOR_ACTION               "COMPRENSION - WAIT_FOR_ACTION"
#define STATE_COMPRENSION_REPOSITION_OBJECT             "COMPRENSION - RESPOSITION_OBJECT"
#define STATE_COMPRENSION_SHOW_RIGHT_OBJECT             "COMPRENSION - SHOW_RIGHT_OBJECT"
#define STATE_COMPRENSION_CONTEXT                       "COMPRENSION - CONTEXT"

#define STATE_COMPRENSION_CORRECT_OBJECT_TO_CART        "COMPRENSION - CORRECT_OBJECT_TO_CART"
#define STATE_COMPRENSION_LEVEL_OUTRO                   "COMPRENSION - LEVEL_OUTRO"
#define STATE_COMPRENSION_OUTRO                         "COMPRENSION - OUTRO"
#define STATE_COMPRENSION_IDLE                          "COMPRENSION - IDLE"


class TABComprensionScene: public TABBaseScene
{
public:
    TABComprensionScene();
    
    //General scene functions
    virtual void update();
    virtual void draw();
    
    virtual void initIntro();
    virtual void initOutro();
    
    void touchDown(ofPoint touchPt);
    void touchMoved(ofPoint touchPt);
    void touchUp(ofPoint touchPt);
    
    //Special scene intro/outro functions
    void initIntroWithInitialInstructions();
    void initLevelIntro(); 
    void initLevelOutro();
    
    //For replaying test instruction (i.e. "Prendi la mela")
    void playTestInstruction();
    
    //Functions for setting up the test
    void setObjects(string correctObjectImgPath, string semanticDistractorObjectImgPath, string distractorAObjectImgPath, string distractorBObjectImgPath);
    void setCustomSounds(string testInstructionFilename, string testContextFilename, string testRightObjectFilename);
    
    //Functions for consistency between different comprension test scenes
    TABInteractiveObject getCorrectCartObject();
    void setInitialCartObjects(vector<TABInteractiveObject> _initialCartObjects);
    
    TABAnimatedObject getCartAnimation();
    void setCartAnimation(TABAnimatedObject animatedObject);
    
    float getNextXBgStart();
    void setXBgStart(float _xBgStart);
    
    //Get test results
    int getResult();
    vector<string> getResultSequence();
    
    //
    void setUserGender(int _userGender); 
    

private:
    virtual void intro();
    virtual void outro();
    
    void initialInstructions();
    void repositionObject();
    void correctObjectToCart();
    
    void initPlayContext();
    void playContext();

    
    bool bObjectsDraggable;
    
    
    vector<TABInteractiveObject> initialCartObjects;
    

    int currentResult;
    vector<string> resultSequence;

    string currentState; 
    
    
    TABCartObject cartObject; //Holds the image that goes on top
    TABAnimatedObject cartAnimation;
    
    
    int bgImageId;
    float xBgStart;
    void drawBg(); 
    
    //Test relative filenames
    vector <TABComprensionObject> comprensionObjects;
    bool bShowObjects; 
    ofSoundPlayer soundPlayer;
    string testInstructionFilename, testContextFilename, testRightObjectFilename;
    
    //
    vector<ofPoint> objectInitialPos;
    ofPoint correctBeforeCartPos;
    ofRectangle correctBeforeCartRect;
    ofPoint correctObjectInsideCartPos; 

    
    //
    ofxTween sceneTween;
    ofxEasingLinear easinglinear;
    
    //
    ofxTween objectsTween;
    ofxEasingBounce easingbounce;
    
    
    vector <ofPoint> comprensionObjectsLastPos;
    void initRepositionObject();
    void initCorrectObjectToCart();
    
    void initShowRightObject(); 
    void showRightObject();
    
    //
    void initInitialInstructions();
    bool bInitialInstructions;
    string initialInstructionFilename, positiveFeedbackMaleFilename, positiveFeedbackFemaleFilename, negativeFeedbackFilename;
    
    bool bLevelIntro; 
    
    
    //
    void initWaitForAction();
    void waitForAction();
    unsigned long long repeatObjectTime;
    
    ofRectangle newRectPos;
    
    int userGender;
    
};

#endif /* defined(__TABLEApp__TABComprensionScene__) */
