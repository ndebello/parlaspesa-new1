//
//  TABEndGameScene.h
//  ParlaSpesa
//
//  Created by Leonardo Amico on 11/02/15.
//
//

#ifndef __ParlaSpesa__TABEndGameScene__
#define __ParlaSpesa__TABEndGameScene__

#define STATE_END_GAME_INTRO                               "END_GAME - INTRO"
#define STATE_END_GAME_ANIMATION                           "END_GAME - ANIMATION"

#define STATE_END_GAME_OUTRO                               "END_GAME - OUTRO"
#define STATE_END_GAME_IDLE                                "END_GAME - IDLE"


#include "TABBaseScene.h"
#include "TABInteractiveObject.h"
#include "TABAnimatedObject.h"
#include "ofxTween.h"

class TABEndGameScene: public TABBaseScene
{
public:
    TABEndGameScene();
    
    //General scene functions
    virtual void update();
    virtual void draw();
    
    virtual void initIntro();
    virtual void initOutro();
    
    virtual void touchDown(ofPoint touchPt);
    virtual void touchMoved(ofPoint touchPt);
    virtual void touchUp(ofPoint touchPt);
    
    
private:
    virtual void intro();
    virtual void outro();
    
    void initAnimation();
    void animation();
    
    string currentState;
    
    //
    ofxTween sceneTween;
    ofxEasingLinear easinglinear;
    
    //
    ofxTween objectsTween;
    ofxEasingBounce easingbounce;
    
    bool bEndGame;
    
    
    //
    ofSoundPlayer soundPlayer;
    
    //
    int bgImageId;
    TABAnimatedObject endGameAnimation;
    
    TABInteractiveObject continueButton;
    
    bool bSceneExited;
    
    
};

#endif /* defined(__ParlaSpesa__TABEndGameScene__) */
