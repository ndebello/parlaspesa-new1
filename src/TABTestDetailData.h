//
//  TABTestDetailData.h
//  TABLEApp
//
//  Created by Leonardo Amico on 20/10/14.
//
//

#import <Foundation/Foundation.h>

@interface TABTestDetailData : NSObject

@property NSString* levelId;
@property NSString* testId;

@property NSString* result; //First attempt/second attempt/failed
@property NSArray* attemptsSequence; //Sequence of the attempts (i.e. {correct}, {semanticDistractor, correct}...)
@property NSString* productionVoiceRecordingPath;
@property NSString* time;

@property NSString* correctObject;
@property NSString* semanticDistractorObject;
@property NSString* distractorAObject;
@property NSString* distractorBObject;

@end
