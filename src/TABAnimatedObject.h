//
//  TABAnimatedObject.h
//  ParlaSpesa
//
//  Created by Leonardo Amico on 27/11/14.
//
//

#ifndef __ParlaSpesa__TABAnimatedObject__
#define __ParlaSpesa__TABAnimatedObject__

#include "TABCartObject.h"


class TABAnimatedObject{
    public:
        TABAnimatedObject(); 
        void draw();
        void update();
        int getCurrentAnimationFrame();
        vector <TABInteractiveObject> frames; //Holds the images composing the animation
    
    private:
        int currentAnimationFrame;
        int nUpdatesPerFrame;
        bool bAnimateForward;
        int currentFrame; 
};

#endif /* defined(__ParlaSpesa__TABAnimatedObject__) */
