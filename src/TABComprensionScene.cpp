
//  TABComprensionScene.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 22/09/14.
//
//

#include "TABComprensionScene.h"
#include "TABImagesData.h"


TABComprensionScene::TABComprensionScene()
{
    sceneKind = "Comprension";
    currentState = STATE_COMPRENSION_IDLE;
    currentResult = COMPRENSION_RESULT_FIRST_ATTEMPT;
    
    resultSequence.resize(0);
    bObjectsDraggable = false;
    bInitialInstructions = false;
    bLevelIntro = false;
    bShowObjects = true;
    xBgStart = 0;
    
    
    ofxXmlSettings gameXml;
    gameXml.load("GameData.xml");
    gameXml.pushTag("AdditionalSceneSettings");
    gameXml.pushTag("ComprensionScene");
    
    //Load sounds
    initialInstructionFilename = gameXml.getValue("InitialInstructions", "");
    positiveFeedbackMaleFilename = gameXml.getValue("PositiveFeedbackMale", "");
    positiveFeedbackFemaleFilename = gameXml.getValue("PositiveFeedbackFemale", "");
    negativeFeedbackFilename = gameXml.getValue("NegativeFeedback", "");
    
    //Load and set images position
    TABImagesData imagesData;
    bgImageId =  imagesData.getImageId(gameXml.getValue("Bg", ""));
    
    //Cart image
    gameXml.pushTag("Cart");
    cartObject = TABCartObject(gameXml.getValue("Top", ""), ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    
    gameXml.pushTag("Animation");
    for (int i=0; i<gameXml.getNumTags("Frame"); i++){
        cartAnimation.frames.push_back(TABInteractiveObject(gameXml.getValue("Frame", "", i), cartObject.getCenter()));
    }
    gameXml.popTag(); //pop animation tag
    
    gameXml.pushTag("InsideCartArea");
    cartObject.setInsideRectMargins(gameXml.getValue("topMarginRel",0.0)*ofGetHeight(), gameXml.getValue("bottomMarginRel",0.0)*ofGetHeight(), gameXml.getValue("leftMarginRel",0.0)*ofGetWidth(), gameXml.getValue("rightMarginRel",0.0)*ofGetWidth());
    gameXml.popTag();
    
    gameXml.pushTag("ActiveCartArea");
    cartObject.setActiveRectMargins(gameXml.getValue("topMarginRel",0.0)*ofGetHeight(), gameXml.getValue("bottomMarginRel",0.0)*ofGetHeight(), gameXml.getValue("leftMarginRel",0.0)*ofGetWidth(), gameXml.getValue("rightMarginRel",0.0)*ofGetWidth());
    gameXml.popTag();
    
    gameXml.popTag(); //pop cart
    
    
    gameXml.pushTag("Objects");
    float xPadding = gameXml.getValue("xPaddingRel", 0.0)*ofGetWidth();
    float yPos = gameXml.getValue("yRelPos", 0.0)*ofGetHeight();
    
    //Objects position is set with a horizontal padding specifying distance between left and right screen margin. Objects are then placed in between that with same distance between each other
    float objectSceneSpace = (ofGetWidth() - 2*xPadding)/4;
    for (int i=0; i<4; i++){
        objectInitialPos.push_back(ofPoint(xPadding + i*objectSceneSpace + objectSceneSpace/2,yPos));
    }
    gameXml.popTag(); //pop objects
    
    
    gameXml.popTag(); //pop Comprension Scene
    gameXml.popTag(); //pop Additional Images
        
    ofBackground(255);
}



void TABComprensionScene::setObjects(string correctObjectImgPath, string semanticDistractorObjectImgPath, string distractorAObjectImgPath, string distractorBObjectImgPath)
{
    
    
    ofRandomize(objectInitialPos);
    
    comprensionObjects.push_back(TABComprensionObject(correctObjectImgPath, objectInitialPos[0], true));
    comprensionObjects.push_back(TABComprensionObject(semanticDistractorObjectImgPath, objectInitialPos[1], false, true));
    comprensionObjects.push_back(TABComprensionObject(distractorAObjectImgPath, objectInitialPos[2]));
    comprensionObjects.push_back(TABComprensionObject(distractorBObjectImgPath, objectInitialPos[3]));
    
    
}





void TABComprensionScene::setCustomSounds(string _testInstructionFilename, string _testContextFilename, string _testRightObjectFilename)
{
    testRightObjectFilename = _testRightObjectFilename;
    testInstructionFilename = _testInstructionFilename;
    testContextFilename = _testContextFilename;
}

void TABComprensionScene::intro()
{
    if (sceneTween.isCompleted()){
        if (!bInitialInstructions){
            playTestInstruction(); //TODO - change name to initial instructions
            
            bObjectsDraggable = true;
            currentState = STATE_COMPRENSION_WAIT_FOR_ACTION;
            ofLog()<<"NEW STATE: "<<currentState;
        }else{
            initInitialInstructions();
        }
    }
}

void TABComprensionScene::repositionObject()
{
    
    for (int i=0; i< comprensionObjects.size(); i++)
    {
        comprensionObjects[i].position = comprensionObjectsLastPos[i] + (comprensionObjects[i].originalPos - comprensionObjectsLastPos[i])*objectsTween.update();
        
    }
    
    if (objectsTween.isCompleted())
    {
        comprensionObjectsLastPos.resize(0);
        if (currentResult == COMPRENSION_RESULT_SECOND_ATTEMPT){
            initPlayContext();
            
            
        }
        else if (currentResult == COMPRENSION_RESULT_FAILED)
        {
            
            initShowRightObject();
            
            
            ofLog()<<"NEW STATE: "<<currentState;
        }
    }
}

void TABComprensionScene::initPlayContext(){
    soundPlayer.loadSound(testContextFilename);
    soundPlayer.play();
    
    bObjectsDraggable = false;
    currentState = STATE_COMPRENSION_CONTEXT;
    ofLog()<<"NEW STATE: "<<currentState;
}

void TABComprensionScene::playContext(){
    if (!soundPlayer.getIsPlaying()){
        bObjectsDraggable = true;
        repeatObjectTime = ofGetElapsedTimeMillis();
        bObjectsDraggable = true;
        currentState = STATE_COMPRENSION_WAIT_FOR_ACTION;
        ofLog()<<"NEW STATE: "<<currentState;
    }
}

void TABComprensionScene::showRightObject(){
    for (int i=0; i < comprensionObjects.size(); i++)
        if (comprensionObjects[i].bCorrect)
            comprensionObjects[i].position = comprensionObjects[i].originalPos +
            (
             ofPoint(cartObject.getInsideRectCenter().x, cartObject.getInsideRectCenter().y - 200) - ofPoint(comprensionObjects[i].width/2, comprensionObjects[i].height/2)
             - comprensionObjects[i].originalPos
             )*objectsTween.update();
    
    if (objectsTween.isCompleted() && !soundPlayer.getIsPlaying()){
        initCorrectObjectToCart();
    }
    
}

void TABComprensionScene::correctObjectToCart()
{
    float tweenVal = objectsTween.update();
    for (int i=0; i < comprensionObjects.size(); i++){
        if (comprensionObjects[i].bCorrect){
            comprensionObjects[i].position = correctBeforeCartRect.position +
            (
             correctObjectInsideCartPos  - ofPoint(comprensionObjects[i].width/2, comprensionObjects[i].height/2)
             - correctBeforeCartRect.position
             )*tweenVal;
            float nextWidth = correctBeforeCartRect.width + (correctBeforeCartRect.width*0.5 - correctBeforeCartRect.width)*tweenVal;
            float nextHeight = correctBeforeCartRect.height + (correctBeforeCartRect.height*0.5 - correctBeforeCartRect.height)*tweenVal;
            
            comprensionObjects[i].setWidth(nextWidth);
            comprensionObjects[i].setHeight(nextHeight);
        }
        else{
            comprensionObjects[i].position = comprensionObjectsLastPos[i] + (comprensionObjects[i].originalPos - comprensionObjectsLastPos[i])*tweenVal;
        }
    }
    
    if (objectsTween.isCompleted())
    {
        currentState = STATE_COMPRENSION_IDLE;
        ofLog()<<"NEW STATE: "<<currentState;
        bCompleted = true;
        
    }
}

void TABComprensionScene::outro()
{
    //    cartAnimation.update();
    if (sceneTween.isCompleted()){
        soundPlayer.stop();
        bDead = true;
        ofLog()<<"SCENE DEAD";
    }
}

void TABComprensionScene::update()
{
    //    for (int i=0; i< comprensionObjects.size(); i++)
    
    
    if (currentState == STATE_COMPRENSION_INTRO)
    {
        this->intro();
        
    }
    
    else if (currentState == STATE_COMPRENSION_INITIAL_INSTRUCTIONS)
    {
        this->initialInstructions();
    }
    
    else if (currentState == STATE_COMPRENSION_WAIT_FOR_ACTION)
    {
        this->waitForAction();
    }
    else if (currentState == STATE_COMPRENSION_REPOSITION_OBJECT)
    {
        this->repositionObject();
        
    }
    else if (currentState == STATE_COMPRENSION_SHOW_RIGHT_OBJECT)
    {
        this->showRightObject();
        
    }
    
    else if (currentState == STATE_COMPRENSION_CONTEXT){
        this->playContext();
    }
    
    else if (currentState == STATE_COMPRENSION_CORRECT_OBJECT_TO_CART)
    {
        this->correctObjectToCart();
        
    }
    else if (currentState == STATE_COMPRENSION_OUTRO)
    {
        this->outro();
        
    }
    
    else if (currentState == STATE_COMPRENSION_LEVEL_OUTRO)
    {
        this->outro(); //Same update function then outro - different drawing
    }
    
}

void TABComprensionScene::draw()
{
    
    float xTranslate = 0;
    if (currentState == STATE_COMPRENSION_INTRO)
        xTranslate = ofGetWidth() - sceneTween.update()*ofGetWidth();
    else if (currentState == STATE_COMPRENSION_OUTRO || currentState == STATE_COMPRENSION_LEVEL_OUTRO)
        xTranslate = -sceneTween.update()*ofGetWidth();
    
    int iCorrect;
    for (int i=0; i< comprensionObjects.size(); i++)
        if (comprensionObjects[i].bCorrect)
            iCorrect = i;
    
    if (currentState == STATE_COMPRENSION_OUTRO)
    {
        ofPushMatrix();
        ofTranslate(xTranslate, 0);
        drawBg();
        
        //Doesn't move with the scene
        ofTranslate(-xTranslate, 0);
        cartAnimation.draw(); //Not animatedx
        cartAnimation.update();
        for (int i=0; i<initialCartObjects.size(); i++){
            initialCartObjects[i].draw();
        }
        ofPushStyle();
        ofEnableAlphaBlending();
        if (currentResult == COMPRENSION_RESULT_FAILED)
            ofSetColor(255, 255*(1-objectsTween.update()));
        
        comprensionObjects[iCorrect].draw(); //And the correct object doesn'r move either
        ofPopStyle();
        ofTranslate(xTranslate, 0);
        //
        for (int i=0; i< comprensionObjects.size(); i++){
            if (i!=iCorrect)
                comprensionObjects[i].draw();
        }
        ofPopMatrix();
        
        cartObject.draw();
        //
    }
    
    else if (currentState == STATE_COMPRENSION_LEVEL_OUTRO)
    {
        ofPushMatrix();
        ofPushStyle();
        ofPushStyle();
        if (bOutroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(ofColor::white, 255*(1-sceneTween.update()));
            
        }else{
            ofTranslate(xTranslate, 0);
        }
        drawBg();
        
        //Leaves with the scene
        cartAnimation.draw();
        if (!bOutroTransitionFade){
            cartAnimation.update();
        }
        
        for (int i=0; i<initialCartObjects.size(); i++){
            initialCartObjects[i].draw();
        }
        //
        
        if (bShowObjects){
            for (int i=0; i< comprensionObjects.size(); i++)
                comprensionObjects[i].draw();
        }
        cartObject.draw();
        ofPopStyle(); 
        ofPopMatrix();
    }
    
    else if (currentState == STATE_COMPRENSION_INTRO && bInitialInstructions ){
        ofPushMatrix();
        ofPushStyle();
        if (bIntroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(ofColor::white, 255*sceneTween.update());
            
        }else{
            ofTranslate(xTranslate, 0);
        }
        drawBg();
        cartAnimation.draw();
        if (!bIntroTransitionFade){
            cartAnimation.update();
        }
        cartObject.draw();
        ofPopStyle();
        ofPopMatrix();
    }
    else if (currentState == STATE_COMPRENSION_INTRO && bLevelIntro){
        ofPushMatrix();
        ofPushStyle();
        if (bIntroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(ofColor::white, 255*sceneTween.update());
            
        }else{
            ofTranslate(xTranslate, 0);
        }
        drawBg();
        for (int i=0; i< comprensionObjects.size(); i++)
            comprensionObjects[i].draw();
        cartAnimation.draw();
        if (!bIntroTransitionFade){
            cartAnimation.update();
        }
        cartObject.draw();
        ofPopStyle();
        ofPopMatrix();
    }
    else if (currentState == STATE_COMPRENSION_INTRO ){
        ofPushMatrix();
        ofTranslate(xTranslate, 0);
        drawBg();
        
        //Doesn't move with the scene
        ofTranslate(-xTranslate, 0);
        cartAnimation.draw(); //Not animated
        cartAnimation.update();
        for (int i=0; i<initialCartObjects.size(); i++){
            initialCartObjects[i].draw();
        }
        ofTranslate(xTranslate, 0);
        //
        
        for (int i=0; i< comprensionObjects.size(); i++)
            comprensionObjects[i].draw();
        ofPopMatrix();
        
        cartObject.draw();
    }
    else if (currentState == STATE_COMPRENSION_INITIAL_INSTRUCTIONS){
        ofPushMatrix();
        ofTranslate(xTranslate, 0);
        drawBg();
        cartAnimation.draw();
        cartObject.draw();
        ofPopMatrix();
    }
    else
    {
        
        ofPushMatrix();
        ofTranslate(xTranslate, 0);
        drawBg();
        
        //Doesn't move with the scene
        ofTranslate(-xTranslate, 0);
        cartAnimation.draw(); //Not animated
        for (int i=0; i<initialCartObjects.size(); i++){
            initialCartObjects[i].draw();
        }
        ofTranslate(xTranslate, 0);
        //
        
        for (int i=0; i< comprensionObjects.size(); i++)
            comprensionObjects[i].draw();
        ofPopMatrix();
        
        cartObject.draw();
        
    }
    
    if (bDebug && currentState != STATE_COMPRENSION_OUTRO){
        ofPushMatrix();
        ofTranslate(xTranslate, 0);
        if (bDebug)
            cartObject.setDebugMode(true);
        ofPushStyle();
        ofNoFill();
        ofSetColor(ofColor::red);
        ofRect(comprensionObjects[iCorrect]);
        ofPopStyle();
        ofPopMatrix();
    }
    
    if (bDebug){
        ofPushStyle();
        ofSetColor(ofColor::blueSteel);
        ofNoFill();
        ofRect(newRectPos);
        ofPopStyle();
    }
}


void TABComprensionScene::touchDown(ofPoint touchPt){
    if (bObjectsDraggable) {
        for (int i=0; i< comprensionObjects.size(); i++)
            if (comprensionObjects[i].hit(touchPt))
                return;
    }
}

void TABComprensionScene::touchMoved(ofPoint touchPt){
    if (bObjectsDraggable) {
        for (int i=0; i< comprensionObjects.size(); i++){
            if (comprensionObjects[i].bDragged){
                comprensionObjects[i].dragged(touchPt);
            }
        }
    }
}

void TABComprensionScene::touchUp(ofPoint touchPt)
{
    if (bObjectsDraggable) {
        for (int i=0; i< comprensionObjects.size(); i++){
            comprensionObjects[i].released(touchPt);
            if (cartObject.isInsideActiveRect(comprensionObjects[i].getCenter())) {
                bObjectsDraggable = false;
                if (comprensionObjects[i].bCorrect){
                    
                    initCorrectObjectToCart();
                    
                    //Play positive feeback
                    if (userGender == GENDER_FEMALE)
                        soundPlayer.loadSound(positiveFeedbackFemaleFilename);
                    else
                        soundPlayer.loadSound(positiveFeedbackMaleFilename);
                    soundPlayer.play();
                    
                    //
                    resultSequence.push_back("Correct");
                    
                }
                else {
                    if (comprensionObjects[i].bSemanticDistractor)
                        resultSequence.push_back("SemanticDistractor");
                    else
                        resultSequence.push_back("Distractor");
                    if (currentResult == COMPRENSION_RESULT_FIRST_ATTEMPT) {
                        
                        currentResult = COMPRENSION_RESULT_SECOND_ATTEMPT;
                        initRepositionObject();
                        
                        ofLog()<<"NEW STATE: "<<currentState;
                        
                        
                        //Play negative feedback
                        soundPlayer.loadSound(negativeFeedbackFilename);
                        soundPlayer.play();
                        
                        
                    }
                    else if (currentResult == COMPRENSION_RESULT_SECOND_ATTEMPT) {
                        currentResult = COMPRENSION_RESULT_FAILED;
                        initRepositionObject();
                        
                        ofLog()<<"NEW STATE: "<<currentState;
                        
                        
                    }
                    
                }
            }
            
        }
    }
}


int TABComprensionScene::getResult(){
    return currentResult;
}

vector<string> TABComprensionScene::getResultSequence(){
    return resultSequence;
}

void TABComprensionScene::initOutro(){
    bCompleted = false;
    soundPlayer.stop(); 
    currentState = STATE_COMPRENSION_OUTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
    objectsTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABComprensionScene::initIntro(){
    currentState = STATE_COMPRENSION_INTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABComprensionScene::initWaitForAction(){
    
    bObjectsDraggable = true;
    currentState = STATE_COMPRENSION_WAIT_FOR_ACTION;
    playTestInstruction();
    ofLog()<<"NEW STATE: "<<currentState;
    
    repeatObjectTime = ofGetElapsedTimeMillis();
    
}

void TABComprensionScene::waitForAction(){
    
    if (ofGetElapsedTimeMillis() - repeatObjectTime > COMPRENSION_REPEAT_OBJECT_INTERVAL){
        playTestInstruction();
        repeatObjectTime = ofGetElapsedTimeMillis();
    }
    
}

void TABComprensionScene::initialInstructions(){
    
    if (!soundPlayer.getIsPlaying()){
        initWaitForAction();
        bShowObjects = true;
        
    }
}

void TABComprensionScene::initRepositionObject()
{
    currentState = STATE_COMPRENSION_REPOSITION_OBJECT;
    for (int i=0; i<comprensionObjects.size(); i++){
        comprensionObjectsLastPos.push_back(comprensionObjects[i].position);
        comprensionObjects[i].bDragged = false;
    }
    objectsTween.setParameters(0, easingbounce, ofxTween::easeOut, 0, 1, OBJECT_ANIMATION_DURATION, 0);
}

void TABComprensionScene::initShowRightObject(){
    currentState = STATE_COMPRENSION_SHOW_RIGHT_OBJECT;
    objectsTween.setParameters(0, easingbounce, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
    soundPlayer.loadSound(testRightObjectFilename);
    soundPlayer.play();
}

void TABComprensionScene::initCorrectObjectToCart()
{
    currentState = STATE_COMPRENSION_CORRECT_OBJECT_TO_CART;
    objectsTween.setParameters(0, easingbounce, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
    
    for (int i=0; i<comprensionObjects.size(); i++){
        comprensionObjectsLastPos.push_back(comprensionObjects[i].position);
        if (comprensionObjects[i].bCorrect){
            float insideObjectWidth = cartObject.getInsideRect().width - comprensionObjects[i].width;
            correctObjectInsideCartPos = ofPoint(cartObject.getInsideRect().x + comprensionObjects[i].width*0.5 + ofRandom(insideObjectWidth), cartObject.getInsideRect().y + cartObject.getInsideRect().height - comprensionObjects[i].height*0.2);
            correctBeforeCartRect = (ofRectangle)comprensionObjects[i];
            //            correctBeforeCartPos = comprensionObjects[i].position;
        }
    }
}


void TABComprensionScene::initIntroWithInitialInstructions()
{
    bInitialInstructions = true;
    initIntro();
}

void TABComprensionScene::initLevelIntro(){
    bLevelIntro = true;
    initIntro();
}

void TABComprensionScene::initInitialInstructions(){
    soundPlayer.loadSound(initialInstructionFilename);
    soundPlayer.play();
    currentState = STATE_COMPRENSION_INITIAL_INSTRUCTIONS;
    bShowObjects = false;
    
}

void TABComprensionScene::initLevelOutro()
{
    soundPlayer.stop();
    bCompleted = false;
    currentState = STATE_COMPRENSION_LEVEL_OUTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

float TABComprensionScene::getNextXBgStart(){
    float bgWidth = TABImagesData::images[bgImageId].getWidth();
    if (xBgStart + ofGetWidth() > bgWidth){
        return xBgStart + ofGetWidth() - bgWidth;
    }
    else{
        return xBgStart + ofGetWidth();
    }
}
void TABComprensionScene::setXBgStart(float _xBgStart){
    xBgStart = _xBgStart;
}

void TABComprensionScene::drawBg(){
    float bgWidth = TABImagesData::images[bgImageId].getWidth();
    float bgHeight = TABImagesData::images[bgImageId].getHeight();
    
    if (bgWidth - xBgStart < ofGetWidth()){
        TABImagesData::images[bgImageId].drawSubsection(0, 0, bgWidth-xBgStart, bgHeight, xBgStart, 0, bgWidth-xBgStart, bgHeight);
        TABImagesData::images[bgImageId].drawSubsection(bgWidth-xBgStart, 0, ofGetWidth()-(bgWidth-xBgStart), bgHeight, 0, 0, ofGetWidth()-(bgWidth-xBgStart), bgHeight);
    }else{
        TABImagesData::images[bgImageId].drawSubsection(0, 0, ofGetWidth(), bgHeight, xBgStart, 0, ofGetWidth(), bgHeight);
    }
}

void TABComprensionScene::setInitialCartObjects(vector<TABInteractiveObject> _initialCartObjects){
    initialCartObjects = _initialCartObjects;
    
}
TABInteractiveObject TABComprensionScene::getCorrectCartObject(){
    for (int i=0; i<comprensionObjects.size(); i++){
        if (comprensionObjects[i].bCorrect)
            return (TABInteractiveObject)comprensionObjects[i];
    }
    return TABInteractiveObject();
}

void TABComprensionScene::playTestInstruction(){
    if (currentState == STATE_COMPRENSION_WAIT_FOR_ACTION){
        soundPlayer.loadSound(testInstructionFilename);
        soundPlayer.play();
        repeatObjectTime = ofGetElapsedTimeMillis();
    }
}

TABAnimatedObject TABComprensionScene::getCartAnimation(){
    return cartAnimation;
}
void TABComprensionScene::setCartAnimation(TABAnimatedObject animatedObject){
    cartAnimation = animatedObject;
}

void TABComprensionScene::setUserGender(int _userGender){
    userGender = _userGender;
}


