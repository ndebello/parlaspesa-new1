//
//  TABCartObject.h
//  TABLEApp
//
//  Created by Leonardo Amico on 23/10/14.
//
//

#ifndef __TABLEApp__TABCartObject__
#define __TABLEApp__TABCartObject__


#include "TABInteractiveObject.h"

class TABCartObject: public TABInteractiveObject{
    
    
public:
    TABCartObject(); 
    TABCartObject(string _imgPath, ofPoint centerPos);
    void setActiveRectMargins(int topMargin, int bottomMargin, int leftMargin, int rightMargin);
    void setInsideRectMargins(int topMargin, int bottomMargin, int leftMargin, int rightMargin);

    void setDebugMode(bool _bDebug);
    
    void draw();
    bool intersectBorder(const ofRectangle& collidingObject);
    bool isInsideActiveRect(ofPoint pt);
    ofPoint getInsideRectCenter();
    ofRectangle getInsideRect(); 
    
private:
    ofRectangle activeRect;
    ofRectangle insideRect;
    int detectionMargin; 
//    bool bDrawActiveRect;
    bool bDebug;
};

#endif /* defined(__TABLEApp__TABCartObject__) */
