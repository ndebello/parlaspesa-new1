//
//  TABToggleButtonObject.cpp
//  ParlaSpesa
//
//  Created by Leonardo Amico on 17/02/15.
//
//

#include "TABToggleButtonObject.h"

TABToggleButtonObject::TABToggleButtonObject():TABButtonObject(){
    bState = false;
};
TABToggleButtonObject::TABToggleButtonObject(string _imgPath, ofPoint centerPos):TABButtonObject(_imgPath,centerPos){
    bState = false;
};

void TABToggleButtonObject::draw()
{

    if (bState){
        if (bImgLoaded)
            TABImagesData::images[imageId].draw(x, y, width, height);
        else {
            ofPushStyle();
            ofSetColor(ofColor::yellowGreen);
            ofRect(*this);
            ofSetColor(255);
            ofPoint stringPos = position + ofPoint(10,10);
            ofDrawBitmapString(imgPath, stringPos);
            ofPopStyle();
        }
    }
}

void TABToggleButtonObject::setState(bool _bState){
    bState = _bState; 
}

bool TABToggleButtonObject::getState(){
    return bState;
}

