
#import "ofAppViewController.h"
#import "ofxiOSExtras.h"
#import "TABMainApp.h"
#import "TABSoundTool.h"

#include "TABImagesData.h"


@interface ofAppViewController()

@property TABSoundTool* soundTool;

@property UITextField* nameTextField;
@property UITextField* ageTextField;

@property (nonatomic) IBOutlet UIButton *gameBackButton;

@end


@implementation ofAppViewController


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSLog(@"ofAppViewController initialization");
        
        self.bGoToLevelsMenu = false;
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.soundTool = [[TABSoundTool alloc] init];
    self.bGameBack = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];

    
    ofLog() << "Sending GL view to back to show text label from storyboard";
    ofxiOSSendGLViewToBack();
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
}




////http://forum.openframeworks.cc/t/hardware-orientation-on-startup-for-ios-with-of-develop/12939/5
//- (id)initWithFrame:(CGRect)frame app:(ofxiOSApp *)app {
//    
//    self = [super initWithFrame:frame app:app];
//    self.glView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    return self;
//}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    if([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending ) {
        
        
    }else{
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        CGRect bounds = CGRectMake(0, 0, screenSize.width, screenSize.height);
        CGPoint center;
        
        bounds.size.width = screenSize.width;
        bounds.size.height = screenSize.height;
        center.x = screenSize.width * 0.5;
        center.y = screenSize.height * 0.5;
        
        self.glView.center = center;
        self.glView.bounds = bounds;
        
        [self.glView updateDimensions];
    }

}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration {
    
    
    if([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending ) {
        [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
        
    }else{
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        CGRect bounds = CGRectMake(0, 0, screenSize.width, screenSize.height);
        CGPoint center;
        
        bounds.size.width = screenSize.width;
        bounds.size.height = screenSize.height;
        center.x = screenSize.width * 0.5;
        center.y = screenSize.height * 0.5;
        
        self.glView.center = center;
        self.glView.bounds = bounds;
        
        [self.glView updateDimensions];
    }

}



- (void)rotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
                            animated:(BOOL)animated {
    
   
    
    
    [super rotateToInterfaceOrientation:interfaceOrientation animated:animated];
    
    // Is the iOS version less than 8?
    if([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending ) {
        
        
    }else{
        CGSize screenSize = [UIScreen mainScreen].bounds.size;
        CGRect bounds = CGRectMake(0, 0, screenSize.width, screenSize.height);
        CGPoint center;
        
        bounds.size.width = screenSize.width;
        bounds.size.height = screenSize.height;
        center.x = screenSize.width * 0.5;
        center.y = screenSize.height * 0.5;
        
        self.glView.center = center;
        self.glView.bounds = bounds;
        
        [self.glView updateDimensions];
    }
}




- (void)loadView {
    [super loadView];
    
    //Preload first fr so to avoid lag between launch image and first OF frame
    TABImagesData imagesData;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        TABImagesData::bIsRetinaEnabled = true;
    } else {
        TABImagesData::bIsRetinaEnabled = false;
    }
    imagesData.loadImages();

    int menuBgId = imagesData.getImageId("menuBg.png", false); //TODO - useless?
//    TABImagesData::images[menuBgId].allocate(2048, 1536, OF_IMAGE_COLOR_ALPHA); 
    
    
    NSLog(@"ofAppViewController view loaded");
    
    TABMainApp* myApp = new TABMainApp();
    myApp->appDelegate = self;
    [self initWithFrame:[[UIScreen mainScreen] bounds] app:myApp ];

}






#pragma mark - custom method can be called from TABMainApp with appropriate casting


- (IBAction)pushGameBackButton:(id)sender {
    self.bGameBack = YES;
}

- (void)hideGameBackButton:(BOOL)hide{
    self.gameBackButton.hidden = hide;
}

- (IBAction)unwindToMenu:(UIStoryboardSegue *)segue{
}


- (void)goToResults{
    [self performSegueWithIdentifier:@"showResults" sender:self];
}

- (void)goToCredits{
    [self performSegueWithIdentifier:@"showCredits" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: @"showCredits"]){
        UIViewController* creditsViewController  = (UIViewController*)segue.destinationViewController;
//        [(UIToolbar*)[creditsViewController.view viewWithTag:1]  setShadowImage:[UIImage new]];
        [(UIToolbar*)[creditsViewController.view viewWithTag:1]  setClipsToBounds:YES];
    }
    
}

#pragma mark - get user data

- (string)getUserName{
    NSString *trimmedNameTextField = [self.nameTextField.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    return trimmedNameTextField.capitalizedString.UTF8String;
}

- (string)getUserAge{
    NSString *trimmedAgeTextField = [self.ageTextField.text stringByTrimmingCharactersInSet:
                                      [NSCharacterSet whitespaceCharacterSet]];
    return trimmedAgeTextField.capitalizedString.UTF8String;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

- (void)hideLoginTextFields:(BOOL)hide{
    
    self.nameTextField.text = @"";
    self.ageTextField.text = @"";
    
    if ([self.view viewWithTag:1] == nil){
        self.nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(284, 485, 282, 62)];
        self.ageTextField = [[UITextField alloc] initWithFrame:CGRectMake(772, 485, 103, 62)];
        self.nameTextField.tintColor = [UIColor blackColor];
        self.ageTextField.tintColor = [UIColor blackColor];
        self.nameTextField.tag = 1;
        self.ageTextField.tag = 2;
        self.ageTextField.font = [UIFont systemFontOfSize:45];
        self.nameTextField.font = [UIFont systemFontOfSize:45];
        self.ageTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.nameTextField.hidden = NO;
        self.ageTextField.hidden = NO;
        [self.view addSubview:self.nameTextField];
        [self.view addSubview:self.ageTextField];
    }
    
    if (hide){
        [self.view viewWithTag:1].hidden = YES;
        [self.view viewWithTag:2].hidden = YES;
    }else{
        [self.view viewWithTag:1].hidden = NO;
        [self.view viewWithTag:2].hidden = NO;
    }

}

- (void)hideKeyboard:(BOOL)hide{
    [self.view endEditing:hide];
}

- (void)keyboardWillHide:(NSNotification *)notification{
    [self screenReactsToKeyboard:notification];
}
- (void)keyboardWillShow:(NSNotification *)notification{
    [self screenReactsToKeyboard:notification];
}

- (void)keyboardDidHide:(NSNotification *)notification{
    self.isKeyboardActive = NO;
}
- (void)keyboardDidShow:(NSNotification *)notification{
    self.isKeyboardActive = YES;
}


- (void)screenReactsToKeyboard:(NSNotification *)notification{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardBeginFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    UIViewAnimationCurve animationCurve = (UIViewAnimationCurve)[[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    NSTimeInterval animationDuration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] integerValue];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.view.frame;
    CGRect keyboardFrameEnd = [self.view convertRect:keyboardEndFrame toView:nil];
    CGRect keyboardFrameBegin = [self.view convertRect:keyboardBeginFrame toView:nil];
    
    if([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] == NSOrderedAscending ) {
        if (orientation == UIInterfaceOrientationLandscapeLeft){
            newFrame.origin.x += (keyboardFrameBegin.origin.y - keyboardFrameEnd.origin.y);
        }
        else if (orientation == UIInterfaceOrientationLandscapeRight){
            newFrame.origin.x -=(keyboardFrameBegin.origin.y - keyboardFrameEnd.origin.y);
        }
        self.view.frame = newFrame;
    }else{
        if (orientation == UIInterfaceOrientationLandscapeLeft){
            newFrame.origin.y -= (keyboardFrameBegin.origin.y - keyboardFrameEnd.origin.y);
        }
        else if (orientation == UIInterfaceOrientationLandscapeRight){
            newFrame.origin.y -=(keyboardFrameBegin.origin.y - keyboardFrameEnd.origin.y);
        }
        self.view.frame = newFrame;
    }

    [UIView commitAnimations];
}


#pragma mark - sound stuff

- (BOOL)recordSound:(NSString*)productionSoundfileName{
    return [self.soundTool record:productionSoundfileName];
}
- (BOOL)playSound:(NSString*)soundfileName{
    return [self.soundTool play:soundfileName isProductionSoundfile:NO];
    
}
- (BOOL)playProductionSound:(NSString*)productionSoundfileName{
    return [self.soundTool play:productionSoundfileName isProductionSoundfile:YES];
}
- (BOOL)stopSound{
    return [self.soundTool stop];
}

- (BOOL)isRecording{
    return [self.soundTool isRecording];
}

- (float)getSoundLevel{
    return [self.soundTool getSoundLevel]; 
}

- (void) updateRecordDetector{
    [self.soundTool updateRecordDetector]; 
}


- (BOOL)getStopRecordingDetected{
    return [self.soundTool getStopRecordingDetected];
}

- (void)resetRecordDetector{
    [self.soundTool resetRecordDetector];
}

- (void)requestRecordPermission{
    [self.soundTool requestRecordPermission]; 
}



@end
