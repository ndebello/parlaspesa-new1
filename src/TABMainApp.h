//Uncomment this line for enable debugging messages and graphics
//#define DEBUG



#pragma once

#include "ofMain.h"
#include "ofxiOS.h"
#include "ofxiOSExtras.h"
#import <UIKit/UIKit.h>

#include "TABMenuScene.h"
#include "TABIntroScene.h"
#include "TABLevelMenuScene.h"
#include "TABComprensionScene.h"
#include "TABProductionScene.h"
#include "TABEndLevelScene.h"
#include "TABEndGameScene.h"

#include "ofxXmlSettings.h"

#include "defines.h"



class TABMainApp;
@protocol ofAppDelegate <NSObject>

- (string)getUserName;
- (string)getUserAge;

- (void)goToResults;
- (void)goToCredits;
//
- (BOOL)recordSound:(NSString*)productionSoundfileName;
- (BOOL)playSound:(NSString*)productionSoundfileName;
- (BOOL)stopSound;
- (BOOL)isRecording;
- (float)getSoundLevel;
- (void)updateRecordDetector;
- (BOOL)getStopRecordingDetected;
- (void)resetRecordDetector;
- (void)requestRecordPermission; 

- (void)hideGameBackButton:(BOOL)hide;
@property bool bGameBack;

- (void)hideLoginTextFields:(BOOL)hide;
- (void)hideKeyboard:(BOOL)hide;
@property bool isKeyboardActive;

@end


class TABMainApp : public ofxiOSApp{
	
    public:
    
    
        ~TABMainApp();
    
        void setup();
        void update();
        void draw();
        void exit();
	
        void touchDown(ofTouchEventArgs & touch);
        void touchMoved(ofTouchEventArgs & touch);
        void touchUp(ofTouchEventArgs & touch);
        void touchDoubleTap(ofTouchEventArgs & touch);
        void touchCancelled(ofTouchEventArgs & touch);

        void lostFocus();
        void gotFocus();
        void gotMemoryWarning();
        void deviceOrientationChanged(int newOrientation);
    
        id<ofAppDelegate> appDelegate; //@property (nonatomic, weak) id <ofAppDelegate> delegate;
    
    
private:
    TABComprensionScene* loadComprension(int levelId, int _userGender, bool bBeginLevel = false);
    TABProductionScene* loadProduction(int levelId);
    TABLevelMenuScene* loadLevelMenu(string userName, string userAge);
    TABMenuScene* loadMenu();
    
    void resetUserGame(); 
    void startGameSession(string userName, string userAge); //TODO - check this, is used for debug mode
    
    string addImageDirPath(string imageFileName);
    string addSoundDirPath(string soundFileName);

    float retinaScale;
    ofRectangle buttonRect;

    
    //
    vector<TABBaseScene*> activeScenes;
    bool bNextSceneAdded;
    
    //
    int currentUserId; 
    int currentLevel;
    string currentLevelTime; 
    int currentTest;
    int nLevels;
    int currentLastAvailableLevel;
    
    
    vector<int> comprensionTestResults;
    
    
    //At the end of each comprension test this element are written in the userResults xml, so to store what objects were present in that test.
    struct comprensionTestImagesType {
        string distractorA;
        string distractorB;
        string semanticDistractor;
        string correct;
    } comprensionTestImages;
    
    
    //For each comprension phase this arrays are passed to the production phase.
    vector<string> comprensionPhaseImages; //TODO - this can be substituted with just the one below, which also holds file paths of images
    vector<TABInteractiveObject> comprensionPhaseObjects;
    vector<int> comprensionPhaseTestId;
    vector<string> comprensionCorrectObjectVoicePaths;
    
    string productionSoundfileName; 
    
    ofxXmlSettings gameXml;
   
    
    bool bFirstSceneLaunched;
    //
    string userName;
    string userAge;
    int userGender;
    
    //
    ofxXmlSettings usersXml;
    void loadUserData();
    
    //
    bool bAdminDebugSession;
    
    //
    string getUserResultsFilename();
    
    //
    bool bIsNewUser;
    
    //
    ofSoundPlayer buttonClickSoundPlayer;
    
    //
    
    
};


