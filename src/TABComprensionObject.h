//
//  TABComprensionObject.h
//  TABLEApp
//
//  Created by Leonardo Amico on 09/10/14.
//
//

#ifndef __TABLEApp__TABComprensionObject__
#define __TABLEApp__TABComprensionObject__

#include "TABInteractiveObject.h"

class TABComprensionObject: public TABInteractiveObject {
public:
    TABComprensionObject(string imgPath, ofPoint centerPos, bool bCorrect = false, bool bSemanticDistractor = false);
    bool bCorrect;
    bool bSemanticDistractor;

    
};

#endif /* defined(__TABLEApp__TABComprensionObject__) */
