//
//  TABSoundTool.h
//  TABLEApp
//
//  Created by Leonardo Amico on 10/10/14.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface TABSoundTool : NSObject <AVAudioPlayerDelegate, AVAudioRecorderDelegate>

- (BOOL)record:(NSString*)productionSoundfileName;
- (BOOL)stop; 
- (BOOL)play:(NSString*)productionSoundfileName isProductionSoundfile:(BOOL)bProductionSoundfile;
- (BOOL)isPlaying;
- (BOOL)isRecording;
- (void)updateRecordDetector;
- (BOOL)getStopRecordingDetected;
- (void)resetRecordDetector;
- (void)requestRecordPermission; 

- (float)getSoundLevel; 

- (instancetype)init; 

@end
