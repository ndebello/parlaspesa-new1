//
//  TABRootNavigationController.m
//  TABLEApp
//
//  Created by Leonardo Amico on 01/10/14.
//
//

#import "TABRootNavigationController.h"

@implementation TABRootNavigationController

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


@end
