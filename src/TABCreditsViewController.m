//
//  TABCreditsViewController.m
//  ParlaSpesa
//
//  Created by Leonardo Amico on 6/11/15.
//
//

#import "TABCreditsViewController.h"
#import "TABSoundTool.h"


@interface TABCreditsViewController ()

@property TABSoundTool* soundTool;


@end

@implementation TABCreditsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.soundTool = [[TABSoundTool alloc] init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Reproduce button sound
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"buttonClick"
                                                         ofType:@"mp3"];
    [self.soundTool play:filePath isProductionSoundfile:NO];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
