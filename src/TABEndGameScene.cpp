//
//  TABEndGameScene.cpp
//  ParlaSpesa
//
//  Created by Leonardo Amico on 11/02/15.
//
//

#include "TABEndGameScene.h"

#include "TABImagesData.h"
#include "ofxXmlSettings.h"

TABEndGameScene::TABEndGameScene(){
    sceneKind = "End Game";
    currentState = STATE_END_GAME_IDLE;
    
    ofxXmlSettings gameXml;
    gameXml.load("GameData.xml");
    gameXml.pushTag("AdditionalSceneSettings");
    gameXml.pushTag("EndGameScene");
    
    TABImagesData imagesData;
    bgImageId =  imagesData.getImageId(gameXml.getValue("Bg", ""));
    
    gameXml.pushTag("Animation");
    for (int i=0; i<gameXml.getNumTags("Frame"); i++){
        endGameAnimation.frames.push_back(TABInteractiveObject(gameXml.getValue("Frame", "", i), ofPoint(ofGetWidth()*0.5, ofGetHeight()*0.5)));
    }
    gameXml.popTag(); //pop animation tag
    
    gameXml.pushTag("ContinueButton");
    continueButton = TABInteractiveObject(gameXml.getValue("Img", ""), ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    continueButton.setPosition(ofPoint(gameXml.getValue("xRelPos",0.0)*ofGetWidth(),gameXml.getValue("yRelPos",0.0)*ofGetHeight()));
    gameXml.popTag();
    
    gameXml.popTag(); //pop End Game Scene
    gameXml.popTag(); //pop Additional Images
    
    
    bSceneExited = false;
    
//    ofSetCircleResolution(255);

    
}

void TABEndGameScene::intro(){
    if (sceneTween.isCompleted())
        initAnimation();
}
void TABEndGameScene::outro(){
    if (sceneTween.isCompleted()){
        bDead = true;
        if (soundPlayer.getIsPlaying())
            soundPlayer.stop();
    }
}
void TABEndGameScene::initIntro(){
    currentState = STATE_END_GAME_INTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABEndGameScene::initOutro(){
    bCompleted = false;
    currentState = STATE_END_GAME_OUTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
}

void TABEndGameScene::initAnimation(){
    currentState = STATE_END_GAME_ANIMATION;
    
    soundPlayer.loadSound("Sounds/FrAc_finalVictory.mp3");
    
    objectsTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 8, END_LEVEL_ANIMATION_DURATION, 0);
    soundPlayer.play();
    
}

void TABEndGameScene::update(){
    
    if (currentState == STATE_END_GAME_INTRO)
    {
        this->intro();
        
    }
    else if (currentState == STATE_END_GAME_ANIMATION)
    {
        this->animation();
        
    }
    else if (currentState == STATE_END_GAME_OUTRO){
        this->outro();
    }
}

void TABEndGameScene::animation(){
    endGameAnimation.update(); 
    
}

void TABEndGameScene::draw(){
    
    ofPushMatrix();
    ofPushStyle();
    
    if (currentState == STATE_END_GAME_INTRO){
        if (bIntroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*sceneTween.update());
        }else{
            float xTranslate = ofGetWidth() - sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
        endGameAnimation.draw();
    }else if (currentState == STATE_END_GAME_OUTRO){
        if (bOutroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*(1 -sceneTween.update()));
        }else{
            float  xTranslate = -sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
        endGameAnimation.draw();
    }else{
        endGameAnimation.draw();
    }
    
    continueButton.draw();
    
    ofPopStyle();
    ofPopMatrix();
}




void TABEndGameScene::touchDown(ofPoint touchPt){
    
    if (continueButton.hit(touchPt)){
    }
    
}
void TABEndGameScene::touchMoved(ofPoint touchPt){
    
}
void TABEndGameScene::touchUp(ofPoint touchPt){
    
    if (!bSceneExited){
        if (continueButton.bDragged){
            continueButton.released(touchPt);
            if (!continueButton.bDragged){
                bCompleted = true;
                bSceneExited = true;
            }
        }
    }
    
}