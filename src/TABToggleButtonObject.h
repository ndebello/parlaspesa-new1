//
//  TABToggleButtonObject.h
//  ParlaSpesa
//
//  Created by Leonardo Amico on 17/02/15.
//
//

#ifndef __ParlaSpesa__TABToggleButtonObject__
#define __ParlaSpesa__TABToggleButtonObject__

#include "TABButtonObject.h"

class TABToggleButtonObject: public TABButtonObject{
    
public:
    TABToggleButtonObject(); 
    TABToggleButtonObject(string _imgPath, ofPoint centerPos);
    void draw(); 
    void setState(bool _bState);
    bool getState();
protected:
    bool bState;
    
};

#endif /* defined(__ParlaSpesa__TABToggleButtonObject__) */
