#include "TABMainApp.h"
#include "TABImagesData.h"




//--------------------------------------------------------------
TABMainApp::~TABMainApp() {
    ofLog() << "TABMainApp destroyed";
    for (int i=0; i<activeScenes.size(); i++){
        delete activeScenes[i];
    }
    
    appDelegate = nil;
}



//--------------------------------------------------------------
void TABMainApp::setup(){
    
    //Enable/disable debug
#ifdef DEBUG
    ofSetLogLevel(OF_LOG_VERBOSE);
#else
    ofSetLogLevel(OF_LOG_SILENT);
#endif
    
    ofBackground(190, 21, 34);
    ofSetFrameRate(60);
    
    TABImagesData imagesData;
    bool bIsRetinaEnabled = ofxiOSGetOFWindow()->isRetinaEnabled(); //Check if we are in retina resolution and set flag accordingly
    TABImagesData::bIsRetinaEnabled = bIsRetinaEnabled; 

    imagesData.loadImages();
    imagesData.getImageId("menuBg.png"); 
    
    gameXml.load("GameData.xml");
    resetUserGame();
    loadUserData();
    
    currentLevelTime = "";
    productionSoundfileName = "";
    
    nLevels =  gameXml.getNumTags("Level");
    
    bAdminDebugSession = false;
    bFirstSceneLaunched = false;
    userGender = -1;
    //
    buttonClickSoundPlayer.loadSound("Sounds/buttonClick.mp3");
    
}

//--------------------------------------------------------------
void TABMainApp::update(){
    
    //Add first scene
    if (!bFirstSceneLaunched){
        bFirstSceneLaunched = true;
        TABMenuScene* menuScene = loadMenu();
        activeScenes.push_back(menuScene);
        [appDelegate requestRecordPermission];
    }
    //If there's no transition between scenes in act, check back menu - else, wait end of transition (until there's just one scene active)
    
    bool bSceneTransition = false;
    if (activeScenes.size() > 1)
        bSceneTransition = true;
    
    //Sync animation on comprension transitions
    if (bSceneTransition && activeScenes[0]->getKind() == "Comprension" && activeScenes[1]->getKind() == "Comprension"){
            TABComprensionScene* compSceneA = (TABComprensionScene*)activeScenes[0]->getScene();
            TABComprensionScene* compSceneB = (TABComprensionScene*)activeScenes[1]->getScene();
        
        compSceneB->setCartAnimation(compSceneA->getCartAnimation()); 
    }
    
    if (!bSceneTransition){
        if ([appDelegate bGameBack]){
            
            
            //Reproduce button sound
            buttonClickSoundPlayer.play();
            
            [appDelegate setBGameBack:NO];
            if (activeScenes.size()>0){
                if (activeScenes[0]->getKind() == "Level menu"){
                    TABLevelMenuScene* levelScene = (TABLevelMenuScene*)activeScenes[0]->getScene();
                    TABMenuScene* newMenuScene = loadMenu();
                    activeScenes.push_back(newMenuScene);
                    newMenuScene->initIntro();
                    levelScene->initOutro();
                    
                }
                else if (activeScenes[0]->getKind() == "Intro"){
                    TABIntroScene* introScene = (TABIntroScene*)activeScenes[0]->getScene();
                    TABMenuScene* newMenuScene = loadMenu();
                    activeScenes.push_back(newMenuScene);
                    newMenuScene->initIntro();
                    introScene->initOutroToMenu(); 
                }
                else{//I'm in comprension or production or end level scene - go back to level menu of current user
                    [appDelegate stopSound];
                    resetUserGame(); //Reset user game data
                    currentLevelTime = "";
                    
                    TABLevelMenuScene* newLevelScene = loadLevelMenu(userName, userAge);
                    if (activeScenes[0]->getKind() == "Comprension"){
                        TABComprensionScene* compScene = (TABComprensionScene*)activeScenes[0]->getScene();
                        compScene->initLevelOutro();
                    }
                    else
                        activeScenes[0]->initOutro();
                    
                    activeScenes.push_back(newLevelScene);
                    newLevelScene->initIntro();
                }
            }
           
            
        }
    }
    
    
    //Update and kill
    for (int i = 0; i<activeScenes.size(); i++){
        activeScenes[i]->update();
        
//        [appDelegate hideGoToLevelsMenuButton:(activeScenes[i]->getKind() == "Level menu")];
        if (activeScenes[i]->getKind() == "Menu") {
            
        }
        if (activeScenes[i]->getKind() == "Production") {
            TABProductionScene* prodScene = (TABProductionScene*)activeScenes[i]->getScene();
            [appDelegate updateRecordDetector]; 
            
            if (prodScene->isRequestRecord()){
                if (![appDelegate isRecording]){
                    
                    stringstream prodFileName;
                    prodFileName<<userName<<"_"<<userAge<<"_"<<currentLevel<<"_"<<prodScene->getCurrentTestId()<<"_"<<ofGetUnixTime()<<".caf";
                    productionSoundfileName = prodFileName.str();
                    [appDelegate recordSound:ofxiOSStringToNSString(productionSoundfileName)];
                }
                if ([appDelegate getStopRecordingDetected])
                    prodScene->stopRecording();
                
            }
            else {
                if ([appDelegate isRecording]){
                    [appDelegate stopSound];
                    [appDelegate resetRecordDetector];
                    
                    prodScene->playObjectName();
                    
                    //Update xml object with new recording filename
                    usersXml.pushTag("root");
                    usersXml.pushTag("User",currentUserId);
                    for (int iLevel = 0; iLevel < nLevels; iLevel++){
                        if (usersXml.getAttribute("Level", "id", -1, iLevel) == currentLevel){
                            usersXml.pushTag("Level", iLevel);
                            int nTests = usersXml.getNumTags("Test");
                            for (int iTest=0; iTest< nTests; iTest++){
                                if (usersXml.getAttribute("Test", "id", -1, iTest) == prodScene->getCurrentTestId()){
                                    usersXml.pushTag("Test", iTest);
                                    usersXml.setValue("VoiceRecording", productionSoundfileName);
                                    usersXml.popTag(); //Pop test
                                }
                            }
                            usersXml.popTag(); //Pop level
                        }
                    }
                    usersXml.popTag(); //Pop user
                    usersXml.popTag(); //Pop root
                }
            }
            
            if ([appDelegate isRecording]){
                prodScene->setSoundLevel([appDelegate getSoundLevel]);
            }

        }
        
        
        if (activeScenes[i]->isCompleted()){
            
            if (activeScenes[i]->getKind() == "Menu"){
                TABMenuScene* menuScene = (TABMenuScene*)activeScenes[i]->getScene();
                string menuButton = menuScene->getSelectedButton();

                
                if (menuButton == "start"){
                    userName = [appDelegate getUserName];
                    userAge = [appDelegate getUserAge];
                    userGender = menuScene->getGender(); 
                    if (userName != "Admin"){
                        bAdminDebugSession = false;
                        if (userName != "" && userAge != "" && userGender != -1){
                            [appDelegate hideKeyboard:YES];
                            if (![appDelegate isKeyboardActive]){
                                [appDelegate hideLoginTextFields:YES];
                                
                                TABIntroScene* newIntroScene = new TABIntroScene();
                                activeScenes.push_back(newIntroScene);
                                menuScene->initOutro();
                                [appDelegate hideGameBackButton:NO];
                                newIntroScene->initIntro(); 
                                
                                
                            }
                        }
                        else{
                            menuScene->resetSelectedButton();
                        }
                    }else{
                        bAdminDebugSession = true;
                        [appDelegate hideKeyboard:YES];
                        if (![appDelegate isKeyboardActive]){
                            [appDelegate hideLoginTextFields:YES];
                            currentUserId = -1;
                            if (userAge == "0" || userAge == "1" || userAge == "2" || userAge == "3" || userAge == "4" ) {
                                currentLevel = ofToInt(userAge);
                                if (currentLevel == nLevels - 1){
                                    TABEndGameScene* endGameScene = new TABEndGameScene();
                                    endGameScene->initIntro();
                                    activeScenes.push_back(endGameScene);
                                }else{
                                    TABEndLevelScene* endLevelScene = new TABEndLevelScene();
                                    endLevelScene->setLevel(currentLevel);
                                    endLevelScene->initIntro();
                                    activeScenes.push_back(endLevelScene);
                                }
                            }
                            
                            else {
                                TABLevelMenuScene* newLevelMenuScene = new TABLevelMenuScene(nLevels);
                                activeScenes.push_back(newLevelMenuScene);
                                newLevelMenuScene->initIntro();
                            }
                            menuScene->initOutro();
                            [appDelegate hideGameBackButton:NO];
                            
                        }

                    }
                }
                else if (menuButton == "credits"){
                    [appDelegate hideKeyboard:YES];
                    if (![appDelegate isKeyboardActive]){
                        menuScene->initOutroToResults();
                        bFirstSceneLaunched = false;
                        [appDelegate goToCredits];
                    }

                    
                }
                else if (menuButton == "results"){
                    [appDelegate hideKeyboard:YES];
                    if (![appDelegate isKeyboardActive]){
                        menuScene->initOutroToResults();
                        bFirstSceneLaunched = false;
                        [appDelegate goToResults];
                    }
                }
            }
            else if (activeScenes[i]->getKind() == "Intro"){
                TABIntroScene* introScene = (TABIntroScene*)activeScenes[i]->getScene();
                TABLevelMenuScene* newLevelMenuScene = loadLevelMenu(userName, userAge);
                activeScenes.push_back(newLevelMenuScene);
                introScene->initOutro();
                newLevelMenuScene->initIntro();
            }

            
            else if (activeScenes[i]->getKind() == "Level menu"){
                TABLevelMenuScene* levelScene = (TABLevelMenuScene*)activeScenes[i]->getScene();
                currentLevel = levelScene->getSelectedLevel();
                ofLog()<<"Level selected: "<<currentLevel;
                
                
                //Start comprension at selected level
                TABComprensionScene* newCompScene = loadComprension(currentLevel, userGender, true);
                
                activeScenes.push_back(newCompScene);
                
                levelScene->initOutro();
                
#ifndef DEBUG
                if (bIsNewUser)
                    newCompScene->initIntroWithInitialInstructions();
                else
                    newCompScene->initLevelIntro();
#else
                newCompScene->initLevelIntro();
#endif

            }
            else if (activeScenes[i]->getKind() == "Comprension"){
                TABComprensionScene* compScene = (TABComprensionScene*)activeScenes[i]->getScene();
                comprensionTestResults[currentTest] = compScene->getResult(); //Put a valid result test
                
                //If test was not failed add current object for being used in the production phase
                if (compScene->getResult() != COMPRENSION_RESULT_FAILED){
                    
                    
                    gameXml.pushTag("Level", currentLevel);
                    
                    int nTests = gameXml.getNumTags("Test");
                    
                    for (int iTest=0; iTest< nTests; iTest++)
                    {
                        if (gameXml.getAttribute("Test", "id", 0, iTest) == currentTest)
                        {
                            gameXml.pushTag("Test", iTest);
                            comprensionPhaseTestId.push_back(iTest);
                            comprensionCorrectObjectVoicePaths.push_back(addSoundDirPath(gameXml.getValue("ProductionObjectName", ""))); //TODO - this doesn't go here
                            gameXml.pushTag("Objects");
                            comprensionPhaseImages.push_back(gameXml.getValue("Correct", "")); //Update vector with all correct image paths of all test done in the comprension phase
                            
                            TABInteractiveObject correctObj = compScene->getCorrectCartObject();
                            comprensionPhaseObjects.push_back(correctObj);
                            
                            gameXml.popTag(); //Pop objects
                            gameXml.popTag(); //Pop test
                        }
                    }
                    
                    gameXml.popTag(); //Pop Level
                    
                }
                
                //Write test result to user stats
                usersXml.pushTag("root");
                usersXml.pushTag("User",currentUserId);
                bool bFoundLevelTag = false;
                for (int iLevel = 0; iLevel < usersXml.getNumTags("Level"); iLevel++){
                    if (usersXml.getAttribute("Level", "id", -1, iLevel) == currentLevel ){
                        
                        
                        usersXml.pushTag("Level", iLevel); //Push level
                        string newTime = usersXml.getValue("Time", "NO");
                        usersXml.popTag();
                        if (currentLevelTime == newTime){
                            usersXml.pushTag("Level", iLevel); //Push level
                            int iTest = usersXml.addTag("Test");
                            usersXml.addAttribute("Test", "id", currentTest, iTest);
                            
                            //Add test info
                            usersXml.pushTag("Test", iTest); //Push test
                            
                            //Add test info from game xml
                            usersXml.addTag("Objects");
                            usersXml.pushTag("Objects");
                            usersXml.setValue("Correct", comprensionTestImages.correct);
                            usersXml.setValue("SemanticDistractor", comprensionTestImages.semanticDistractor);
                            usersXml.setValue("Distractor", comprensionTestImages.distractorA, 0);
                            usersXml.setValue("Distractor", comprensionTestImages.distractorB, 1);
                            usersXml.popTag();//Pop objects

                            
                            usersXml.setValue("Result", compScene->getResult());
                            for (int iAttemps = 0; iAttemps < compScene->getResultSequence().size(); iAttemps++){
                                usersXml.setValue("Attempt", compScene->getResultSequence()[iAttemps], iAttemps);
                            }
                            
                            usersXml.popTag(); //Pop Test
                            usersXml.popTag(); //Pop Level
                            bFoundLevelTag = true;
                            break;
                        }
                    }
                }
                if (!bFoundLevelTag){//Add level - this is the first comprension test of a new one
                    
                    int newLevelId = usersXml.addTag("Level");
                    usersXml.addAttribute("Level", "id", currentLevel, newLevelId);
                    
                    usersXml.pushTag("Level", newLevelId); //Push level
                    
                    //If new level it could be a new session. Add time
                    stringstream levelTime;
                    levelTime<<ofGetHours()<<":"<<ofGetMinutes()<<" "<<ofGetDay()<<"-"<<ofGetMonth()<<"-"<<ofGetYear();
                    currentLevelTime = levelTime.str();
                    usersXml.setValue("Time", levelTime.str());
                    
                    int iTest = usersXml.addTag("Test");
                    usersXml.addAttribute("Test", "id", currentTest, iTest);
                    
                    usersXml.pushTag("Test", iTest); //Push test
                    
                    //Add test info from game xml
                    usersXml.addTag("Objects");
                    usersXml.pushTag("Objects");
                    usersXml.setValue("Correct", comprensionTestImages.correct);
                    usersXml.setValue("SemanticDistractor", comprensionTestImages.semanticDistractor);
                    usersXml.setValue("Distractor", comprensionTestImages.distractorA, 0);
                    usersXml.setValue("Distractor", comprensionTestImages.distractorB, 1);
                    usersXml.popTag();//Pop objects
                    
                    usersXml.setValue("Result", compScene->getResult());
                    for (int iAttemps = 0; iAttemps < compScene->getResultSequence().size(); iAttemps++){
                        usersXml.setValue("Attempt", compScene->getResultSequence()[iAttemps], iAttemps);
                    }
                    
                    usersXml.popTag(); //Pop Test
                    usersXml.popTag(); //Pop Level
                }

                usersXml.popTag(); //Pop User
                usersXml.popTag(); //Pop root
                
                
                //Check whether is to finish game or pass to production
                bool bQuitGame = false;
                bool bPassToProduction = false;

                //Check if time to pass to the production stage
                int nFailed = 0;
                int nSuccess = 0;
                for (int i=0; i<comprensionTestResults.size(); i++){
                    if (comprensionTestResults[i] == COMPRENSION_RESULT_FAILED){
                        nFailed++;
                        if (nFailed == 4){
                            ofLog()<<"4 Tests failed -> Quit game ";
                            bQuitGame = true;
                        }
                    }
                    if (comprensionTestResults[i] == COMPRENSION_RESULT_FIRST_ATTEMPT){
                        nSuccess++;
                        if (nSuccess == 4){
                            ofLog()<<"4 Tests success -> Pass to Production";
                            bPassToProduction = true;
                        }
                    }
                }
                
                if (bPassToProduction){
                    TABProductionScene* newProdScene = loadProduction(currentLevel);
                    activeScenes.push_back(newProdScene);
                    
                    compScene->initLevelOutro();
#ifdef DEBUG
                    newProdScene->initIntro();
#else
                    newProdScene->initIntroWithInitialInstructions();
#endif
                    
                }
                else if (bQuitGame){
                    TABLevelMenuScene* newLevelScene = loadLevelMenu(userName, userAge);
                    resetUserGame();
                    activeScenes.push_back(newLevelScene);
                    
                    newLevelScene->initIntro(); 
                    compScene->initLevelOutro(); //Outro previous scene
                    
                    
                }
                else{
                    
                    TABComprensionScene* newCompScene =  loadComprension(currentLevel, userGender);
                    newCompScene->setXBgStart(compScene->getNextXBgStart());
                    activeScenes.push_back(newCompScene);
                    
                    compScene->initOutro(); //Outro previous scene
                    newCompScene->initIntro();
                }
            }
            else if (activeScenes[i]->getKind() == "Production") {
                
                //
                if (currentLevel == nLevels - 1){
                    TABEndGameScene* endGameScene = new TABEndGameScene();
                    endGameScene->initIntro();
                    activeScenes.push_back(endGameScene);

                }else{
                    TABEndLevelScene* endLevelScene = new TABEndLevelScene();
                    endLevelScene->setLevel(currentLevel);
                    endLevelScene->initIntro();
                    activeScenes.push_back(endLevelScene);
                    
                }
                TABProductionScene* prodScene = (TABProductionScene*)activeScenes[i]->getScene();
                
                prodScene->initOutro(); //Outro previous scene
                
                //Level finished - Save user data xml
                bool bSaved;
                if (!bAdminDebugSession){
                    bSaved = usersXml.save(ofxiOSGetDocumentsDirectory()+getUserResultsFilename());
                }
#ifdef DEBUG
                ofLog() << "Level finished - Saving userXml: \n\n\n";
                if (bAdminDebugSession){
                    ofLog() << "Admin session - userXml will not be saved successfull\n\n\n";
                }else{
                    if (bSaved)
                        ofLog() << "Saving userXml successfull\n\n\n";
                    else
                        ofLog() << "Saving userXml not successfull\n\n\n";
                    ofxXmlSettings xml;
                    xml.load(ofxiOSGetDocumentsDirectory()+getUserResultsFilename());
                    string xmlString;
                    xml.copyXmlToString(xmlString);
                    cout<<"userXml: \n"<<xmlString<<"\n\n\n";
                }
#endif
                
                //If new level update currentLastAvailableLevel
                if (currentLevel+1 > currentLastAvailableLevel)
                    currentLastAvailableLevel = currentLevel+1;
                
            }
            
            else if (activeScenes[i]->getKind() == "End Level"){
                
                TABEndLevelScene* endLevelScene = (TABEndLevelScene*)activeScenes[i]->getScene();
                
                //Clear comprension and production scenes
                resetUserGame();
                
                currentLevel++;
                //Start comprension at selected level
                TABComprensionScene* newCompScene = loadComprension(currentLevel, userGender, true);
                activeScenes.push_back(newCompScene);
                
                endLevelScene->initOutro();
                newCompScene->initLevelIntro();
            }
            
            else if (activeScenes[i]->getKind() == "End Game"){
                TABEndGameScene* endGameScene = (TABEndGameScene*)activeScenes[i]->getScene();
                resetUserGame();
                
                TABLevelMenuScene* newLevelScene = new TABLevelMenuScene(nLevels, nLevels-1);
                activeScenes.push_back(newLevelScene);
                
                endGameScene->initOutro();
                newLevelScene->initIntro();
                
            }
        }
        
        if (activeScenes[i]->isDead()){
            delete activeScenes[i];
            activeScenes.erase(activeScenes.begin() + i);
            
        }
        
    }
#ifdef DEBUG
    for (int i=0; i<activeScenes.size(); i++){
        activeScenes[i]->bDebug = true;
    }
#endif
}

//--------------------------------------------------------------
void TABMainApp::draw(){    
    for (int i = 0; i<activeScenes.size(); i++)
        activeScenes[i]->draw();
    
#ifdef DEBUG
    ofPushStyle();
    ofSetColor(0); 
    ofDrawBitmapString(ofToString(ofGetFrameRate()), ofPoint(20,20));
    ofPopStyle();
#endif
}

//--------------------------------------------------------------
void TABMainApp::exit(){
    ofLog() << "TABMainApp exited";
    
    //TODO - save all user data
    
}

//--------------------------------------------------------------
void TABMainApp::touchDown(ofTouchEventArgs & touch){
    if (touch.id == 0){
        for (int i = 0; i<activeScenes.size(); i++)
            activeScenes[i]->touchDown(ofPoint(touch.x, touch.y));
    }
    
    
    
}

//--------------------------------------------------------------
void TABMainApp::touchMoved(ofTouchEventArgs & touch){
    if (touch.id == 0){
        for (int i = 0; i<activeScenes.size(); i++)
            activeScenes[i]->touchMoved(ofPoint(touch.x, touch.y));
    }
}

//--------------------------------------------------------------
void TABMainApp::touchUp(ofTouchEventArgs & touch){
    if (touch.id == 0){
        for (int i = 0; i<activeScenes.size(); i++)
            activeScenes[i]->touchUp(ofPoint(touch.x, touch.y));
    }
    
    
}

//--------------------------------------------------------------
void TABMainApp::touchDoubleTap(ofTouchEventArgs & touch){
    if (touch.id == 2) {
        for (int i = 0; i<activeScenes.size(); i++)
            if (activeScenes[i]->getKind() == "Comprension"){
                TABComprensionScene* comprensionScene =  (TABComprensionScene *)activeScenes[i]->getScene();
                comprensionScene->playTestInstruction();
            }
        //TODO - check this, may be buggy - and it's useless
//            else if (activeScenes[i]->getKind() == "Production"){
//                TABProductionScene* productionScene =  (TABProductionScene *)activeScenes[i]->getScene();
//                productionScene->togglePause();
//            }
    }

}

//--------------------------------------------------------------
void TABMainApp::touchCancelled(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void TABMainApp::lostFocus(){

}

//--------------------------------------------------------------
void TABMainApp::gotFocus(){

}

//--------------------------------------------------------------
void TABMainApp::gotMemoryWarning(){

}

//--------------------------------------------------------------
void TABMainApp::deviceOrientationChanged(int newOrientation){

    //CGRect bounds = [(id)[(id)ofxiOSGetAppDelegate() getViewController] bounds];
    //ofLog() << "1 bounds:" << bounds.size.width << ", " << bounds.size.height;
    
    ofLog() << "2 device orientation changed: " << newOrientation;
    //ofSetOrientation((ofOrientation)newOrientation);
    
    
}

TABComprensionScene* TABMainApp::loadComprension(int levelId, int _userGender, bool bBeginLevel)
{
    
    gameXml.pushTag("Level", levelId);
    

    int nTests = gameXml.getNumTags("Test");
    
    if (bBeginLevel){
        comprensionTestResults.assign(nTests, -1);
    }
    
    int nTestsCompleted = 0;
    for (int i=0; i<comprensionTestResults.size(); i++)
        if (comprensionTestResults[i] != -1)
            nTestsCompleted++;
    
    //If done all tests, reset all of them which were not done correctly and
    if (nTests == nTestsCompleted){
        for (int i=0; i<comprensionTestResults.size(); i++)
            if (comprensionTestResults[i] != COMPRENSION_RESULT_FIRST_ATTEMPT){
                comprensionTestResults[i] = -1;
                nTestsCompleted--;
            }
    }
    
    currentTest = ofRandom(0, nTests-1 - nTestsCompleted);
    //Check if the test was already done - if yes update currentTest with next one
    while (currentTest < nTests){
        if (comprensionTestResults[currentTest] != -1)
            currentTest++;
        else
            break;
    }

    ofLog()<<"Load comprension - level: "<<currentLevel<<"; test: "<<currentTest;

    
    TABComprensionScene* compScene = new TABComprensionScene;
    
    compScene->setUserGender(_userGender);
    
    for (int iTest=0; iTest< nTests; iTest++)
    {
        if (gameXml.getAttribute("Test", "id", 0, iTest) == currentTest)
        {
            gameXml.pushTag("Test", iTest);

            gameXml.pushTag("Objects");
            string path = gameXml.getValue("Context", "");
            
            comprensionTestImages.correct = gameXml.getValue("Correct", "");
            comprensionTestImages.semanticDistractor = gameXml.getValue("SemanticDistractor", "");
            comprensionTestImages.distractorA = gameXml.getValue("Distractor", "", 0);
            comprensionTestImages.distractorB = gameXml.getValue("Distractor", "", 1);
            
            
            compScene->setObjects(
                                gameXml.getValue("Correct", ""),
                                gameXml.getValue("SemanticDistractor", ""),
                                gameXml.getValue("Distractor", "",0),
                                gameXml.getValue("Distractor", "",1));
            
            gameXml.popTag(); //Pop object tab
            compScene->setCustomSounds(
                                       addSoundDirPath(gameXml.getValue("ComprensionObjectInstruction", "")),
                                       addSoundDirPath(gameXml.getValue("ComprensionObjectContext", "")),
                                       addSoundDirPath(gameXml.getValue("ComprensionObjectSolution", "")));
            
            
            gameXml.popTag(); //Pop Test tag


        }
        
    }
    
    gameXml.popTag(); //Pop Level tag
    
    compScene->setInitialCartObjects(comprensionPhaseObjects);
    
    return compScene;

}

TABProductionScene* TABMainApp::loadProduction(int levelId)
{


    TABProductionScene* tabProd = new TABProductionScene();
    tabProd->setImagePaths(comprensionPhaseImages);
    tabProd->setTestId(comprensionPhaseTestId);
    tabProd->setObjectVoicePaths(comprensionCorrectObjectVoicePaths); 
    return tabProd;
}

TABLevelMenuScene* TABMainApp::loadLevelMenu(string userName, string userAge){
    loadUserData(); //Reload user data, for cleaning uncompleted data from the ofxXml object
    usersXml.pushTag("root"); //push root
    int nUsers = usersXml.getNumTags("User");
    currentUserId = -1; //userXml index number for the tag where to save data (whether a new tag or one already present)
    bIsNewUser = true;
    for (int i=0; i < nUsers; i++) {
        string xmlUserName = usersXml.getAttribute("User", "name", "", i);
        string xmlUserAge = usersXml.getAttribute("User", "age", "", i);
        if (userName == xmlUserName){
            if (userAge == xmlUserAge){
                currentUserId = i;
                bIsNewUser = false;
                break;
            }
        }
    }
    if (bIsNewUser){//Is a new user
        currentUserId = nUsers;
        usersXml.addTag("User");
        usersXml.addAttribute("User", "name", userName, currentUserId);
        usersXml.addAttribute("User", "age", userAge, currentUserId);
    }
    
    usersXml.pushTag("User",currentUserId);
    
    currentLastAvailableLevel = 0;
    for (int iLevel=0; iLevel< usersXml.getNumTags("Level"); iLevel++){
        if (usersXml.getAttribute("Level", "id", -1, iLevel)+1 > currentLastAvailableLevel){
            currentLastAvailableLevel = usersXml.getAttribute("Level", "id", -1, iLevel)+1;
        }
    }
    usersXml.popTag(); //pop users
    usersXml.popTag(); //pop root
    
    if (currentLastAvailableLevel == nLevels)
        currentLastAvailableLevel = nLevels -1;
    
    TABLevelMenuScene* levelScene = new TABLevelMenuScene(nLevels, currentLastAvailableLevel);
    return levelScene; 
    
}

TABMenuScene* TABMainApp::loadMenu(){
    [appDelegate hideLoginTextFields:NO];
    [appDelegate hideGameBackButton:YES];
    userGender = -1;
    
    TABMenuScene* menuScene = new TABMenuScene();
    return menuScene;
    
}

void TABMainApp::startGameSession(string userName, string userAge){
    
    nLevels =  gameXml.getNumTags("Level");
    if (userName == "admin") {//This is for debugging session
        currentUserId = -1;
        bAdminDebugSession = true;
        //
        if (userAge == "0" || userAge == "1" || userAge == "2" || userAge == "3" || userAge == "4" ) {
            currentLevel = ofToInt(userAge);
            if (currentLevel == nLevels - 1){
                TABEndGameScene* endGameScene = new TABEndGameScene();
                endGameScene->initIntro();
                activeScenes.push_back(endGameScene);
            }else{
                TABEndLevelScene* endLevelScene = new TABEndLevelScene();
                endLevelScene->setLevel(currentLevel);
                endLevelScene->initIntro();
                activeScenes.push_back(endLevelScene);
            }
        }
        
        else {
            TABLevelMenuScene* levelScene = new TABLevelMenuScene(nLevels);
            activeScenes.push_back(levelScene);
        }
        
    }
    
    else { //Normal session
        usersXml.pushTag("root"); //push root
        int nUsers = usersXml.getNumTags("User");
        currentUserId = -1; //userXml index number for the tag where to save data (whether a new tag or one already present)
        bool bIsNewUser = true;
        for (int i=0; i < nUsers; i++) {
            string xmlUserName = usersXml.getAttribute("User", "name", "", i);
            string xmlUserAge = usersXml.getAttribute("User", "age", "", i);
            if (userName == xmlUserName){
                if (userAge == xmlUserAge){
                    currentUserId = i;
                    bIsNewUser = false;
                    break;
                }
            }
        }
        if (bIsNewUser){//Is a new user
            currentUserId = nUsers;
            usersXml.addTag("User");
            usersXml.addAttribute("User", "name", userName, currentUserId);
            usersXml.addAttribute("User", "age", userAge, currentUserId);
        }
        
        usersXml.pushTag("User",currentUserId);
        
        currentLastAvailableLevel = 0;
        for (int iLevel=0; iLevel< usersXml.getNumTags("Level"); iLevel++){
            if (usersXml.getAttribute("Level", "id", -1, iLevel)+1 > currentLastAvailableLevel){
                currentLastAvailableLevel = usersXml.getAttribute("Level", "id", -1, iLevel)+1;
            }
        }
        usersXml.popTag(); //pop users
        usersXml.popTag(); //pop root
        
        if (currentLastAvailableLevel == nLevels)
            currentLastAvailableLevel = nLevels -1;
        
        TABLevelMenuScene* levelScene = new TABLevelMenuScene(nLevels, currentLastAvailableLevel);
        activeScenes.push_back(levelScene);
//        [appDelegate hideGoToLevelsMenuButton:YES];
    }
    
}

string TABMainApp::addSoundDirPath(string soundFileName)
{
    stringstream ss;
    ss<<"Sounds/"<<soundFileName;
    return ss.str();
    
}

void TABMainApp::loadUserData(){
    //First check if present in document directory - if not get the test one
    if (!usersXml.load(ofxiOSGetDocumentsDirectory()+getUserResultsFilename()))
    usersXml.load("UserResultsData.xml");
}

void TABMainApp::resetUserGame(){
    comprensionPhaseImages.resize(0);
    comprensionPhaseObjects.resize(0);
    comprensionPhaseTestId.resize(0);
    comprensionCorrectObjectVoicePaths.resize(0);
    
}

string TABMainApp::getUserResultsFilename(){ //Depend on device
    string vendorId = ofxiOSNSStringToString([[[UIDevice currentDevice] identifierForVendor] UUIDString]);
    return vendorId+"_UserResultsData.xml";
}




