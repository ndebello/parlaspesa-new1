//
//  TABUsersViewController.h
//  TABLEApp
//
//  Created by Leonardo Amico on 28/10/14.
//
//

#import <UIKit/UIKit.h>
#import "TABUserDetailsData.h"


@interface TABUsersViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>



@end
