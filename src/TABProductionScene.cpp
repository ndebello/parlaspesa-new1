//
//  TABProductionScene.cpp
//  TABLEApp
//
//  Created by Leonardo Amico on 22/09/14.
//
//

#include "TABProductionScene.h"
#include "TABImagesData.h"


TABProductionScene::TABProductionScene()
{
    sceneKind = "Production";
    currentState = STATE_PRODUCTION_IDLE;
    
    //Position for objects trajectories in different stages
    objectCartPos = ofPoint(ofGetWidth()*0.29, ofGetHeight()*0.92);
    objectBeltPos = ofPoint(ofGetWidth()*0.38, ofGetHeight()*0.49);
    objectScannerPos = ofPoint(ofGetWidth()*0.58, ofGetHeight()*0.52);
    objectPeakPos = ofPoint(0,ofGetHeight()*0.26);
    objectAwayPos = ofPoint(ofGetWidth()*1.08, ofGetHeight()*0.66);
    
    bInitialInstructions = false;
    bRequestRecord = false;
    soundLevel = -100;
    
    bPause = false;

    
    ofxXmlSettings gameXml;
    gameXml.load("GameData.xml");
    gameXml.pushTag("AdditionalSceneSettings");
    gameXml.pushTag("ProductionScene");
    
    TABImagesData imagesData;
    bgImageId =  imagesData.getImageId(gameXml.getValue("Bg", ""));
    topImageId =  imagesData.getImageId(gameXml.getValue("Top", ""));
    gameXml.pushTag("RegisterDisplayOn");
    registerDisplayOn = TABInteractiveObject(gameXml.getValue("Img", ""), ofPoint(gameXml.getValue("xRelPos", 0.0)*ofGetWidth(),gameXml.getValue("yRelPos", 0.0)*ofGetHeight()));
    gameXml.popTag();
    
    recordVoiceSignalSoundfile = gameXml.getValue("RecordVoiceSignal", "");
    gameXml.pushTag("Meter");
    for (int i=0; i<gameXml.getNumTags("Frame"); i++){
        meter.push_back(TABInteractiveObject(gameXml.getValue("Frame", "", i), ofPoint(0.24*ofGetWidth(),0.24*ofGetHeight())));//TODO - this should in the xml
    }
    gameXml.popTag(); //pop Meter
    
    initialInstructionsFilename = gameXml.getValue("InitialInstructions", "");
    testInstructionsFilename = gameXml.getValue("TestInstructions", "");
    
    gameXml.popTag(); //pop Prod Scene
    gameXml.popTag(); //pop Additional Images
    
    ofBackground(255);
}


void TABProductionScene::draw()
{
    ofPushMatrix();
    ofPushStyle();
    if (currentState == STATE_PRODUCTION_INTRO){
        if (bIntroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*sceneTween.update());
        }else{
            float xTranslate = ofGetWidth() - sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
    }else if (currentState == STATE_PRODUCTION_OUTRO){
        if (bOutroTransitionFade){
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, 255*(1 -sceneTween.update()));
        }else{
            float  xTranslate = -sceneTween.update()*ofGetWidth();
            ofTranslate(xTranslate, 0);
        }
    }

    
    TABImagesData::images[bgImageId].draw(0,0);
    
    for (int i=0; i<productionObjects.size(); i++)
        productionObjects[i].draw();
    
    TABImagesData::images[topImageId].draw(0,0);
    
    if (currentState == STATE_RECORD_VOICE_SIGNAL){
        registerDisplayOn.draw();
    }

    if (currentState == STATE_RECORD_VOICE_SIGNAL || currentState == STATE_RECORD_VOICE)
    {
        int meterIndex = min(max(0,(int)ofMap(soundLevel, -50, 0, 0, 10)), 10);
        
        meter[meterIndex].draw();
    }
    
    ofPopStyle();
    ofPopMatrix();
    
    if (bDebug){
        ofPushStyle();
        ofNoFill();
        ofSetColor(ofColor::red);
        ofBeginShape();
        for (int i=0; i < objectTrajectory.getVertices().size(); i++){
            ofVertex(objectTrajectory.getVertices()[i]);
        }
        ofEndShape();
        ofPopStyle();
    }
}

void TABProductionScene::update()
{
    if (currentState == STATE_PRODUCTION_INTRO)
    {
        this->intro();
    }
    
    else if (currentState == STATE_PRODUCTION_INITIAL_INSTRUCTIONS)
    {
        this->initialInstructions();
    }
    
    else if (currentState == STATE_PRODUCTION_INSTRUCTIONS)
    {
        this->instructions();
    }
    else if (currentState == STATE_PRODUCTION_CART_TO_BELT){
        this->objectCartToBelt();
    }
    else if (currentState == STATE_PRODUCTION_BELT_TO_SCANNER){
        this->objectBeltToScanner();
    }
    else if (currentState == STATE_RECORD_VOICE_SIGNAL){
        this->recordVoiceSignal();
    }
    else if (currentState == STATE_PRODUCTION_SCANNER_TO_AWAY){
        this->objectScannerAway();
    }
    
    
    else if (currentState == STATE_RECORD_VOICE)
    {
        this->recordVoice();
    }
    
    else if (currentState == STATE_PLAY_OBJECT_NAME)
    {
        this->playObject();
    }

    else if (currentState == STATE_PRODUCTION_OUTRO)
    {
        this->outro();
        
    }
    
}

void TABProductionScene::intro()
{
    if (sceneTween.isCompleted())
    {
        if (bInitialInstructions){
            initInitialInstruction();
            bInitialInstructions = false;
        }
        else{
            initObjectCartToBelt();
        }
    }
}

void TABProductionScene::outro()
{
    if (sceneTween.isCompleted()){
        bDead = true;
        ofLog()<<"SCENE DEAD";
    }
}

void TABProductionScene::initOutro()
{
    bCompleted = false;
    currentState = STATE_PRODUCTION_OUTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
    
    
}

void TABProductionScene::initIntro()
{
    currentState = STATE_PRODUCTION_INTRO;
    sceneTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, INTRO_OUTRO_SCENE_DURATION, 0);
    
    
}

void TABProductionScene::setImagePaths(vector<string> imagePaths)
{
    
    float xRange = (ofGetWidth()*0.06 -ofGetWidth()*0.3)/imagePaths.size();
    for (int i=0; i<imagePaths.size(); i++){
        TABInteractiveObject obj = TABInteractiveObject(imagePaths[i], objectCartPos + ofPoint(xRange*i,0));
        obj.scaleFromCenter(1.4);
        productionObjects.push_back(obj);
    }
    
}

void TABProductionScene::setTestId(vector<int> testIds)
{
    productionTestId = testIds;
    for (int i=0; i<testIds.size(); i++)
    {
        productionTestId.push_back(testIds[i]);
    }
    
}

void TABProductionScene::recordVoice()
{
    if (ofGetElapsedTimeMillis() - recordTimer > PRODUCTION_RECORDING_TIME){
        bRequestRecord = false;
        currentState = STATE_PRODUCTION_IDLE;
    }
}



void TABProductionScene::initIntroWithInitialInstructions()
{
    initIntro();
    bInitialInstructions = true;

}

void TABProductionScene::initInitialInstruction()
{
    currentState = STATE_PRODUCTION_INITIAL_INSTRUCTIONS;
    soundPlayer.loadSound(initialInstructionsFilename);
    soundPlayer.play();
    
    //TODO - Tween for moving objects while giving instructions - TODO set animation duration constant
    objectsTween.setParameters(0, easingbounce, ofxTween::easeOut, 0, 1, 3000, 0);
}

void TABProductionScene::initialInstructions()
{
    objectsTween.update();
    if (objectsTween.isCompleted()){
        initObjectCartToBelt();
    }
}


void TABProductionScene::instructions()
{
    if (!soundPlayer.getIsPlaying()){
        initObjectCartToBelt();
    }
    
}

int TABProductionScene::getCurrentTestId(){
    return productionTestId[0];
}

bool TABProductionScene::isRequestRecord(){
    return bRequestRecord;
}

void TABProductionScene::stopRecording(){
    bRequestRecord = false;
    currentState = STATE_PRODUCTION_IDLE;
}

void TABProductionScene::playObjectName(){
    currentState = STATE_PLAY_OBJECT_NAME;
    //TODO - Tween for moving objects while giving instructions
    objectsTween.setParameters(0, easingbounce, ofxTween::easeOut, 0, 1, 1000, 0);
    
    soundPlayer.loadSound(productionObjectsVoicePaths[0]);
    soundPlayer.play();
}

void TABProductionScene::playObject(){
    objectsTween.update();
    if (objectsTween.isCompleted()){
        initObjectScannerAway();
    }
}

void TABProductionScene::setObjectVoicePaths(vector<string> objectVoicePaths){
    productionObjectsVoicePaths = objectVoicePaths;
}

void TABProductionScene::setSoundLevel(float _soundLevel){
    soundLevel = _soundLevel;
}

void TABProductionScene::initObjectCartToBelt(){
    currentState = STATE_PRODUCTION_CART_TO_BELT;
    insideCartObject = (ofRectangle)productionObjects[0];
    
    objectPeakPos.x = 0.5*(objectBeltPos.x - objectCartPos.x);
    objectTrajectory.clear();
    objectTrajectory.quadBezierTo(insideCartObject.getCenter(), objectPeakPos, objectBeltPos,50);
    
    objectsTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, 1000, 0); //TODO - time here (1000) should be set from somewhere else's constant, not hardcoded here
    
    
    //
    soundPlayer.loadSound(testInstructionsFilename);
    soundPlayer.play();
}
void TABProductionScene::initObjectBeltToScanner(){
    currentState = STATE_PRODUCTION_BELT_TO_SCANNER;
    
    objectTrajectory.clear();
    objectTrajectory.quadBezierTo(objectBeltPos, objectScannerPos, objectScannerPos,50);
    //
    objectsTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, 1000, 0); //TODO - time here (1000) should be set from somewhere else's constant, not hardcoded here
    
}

void TABProductionScene::initRecordVoice(){
    currentState = STATE_RECORD_VOICE;
    recordTimer = ofGetElapsedTimeMillis();
    bRequestRecord = true;
    
    //Show baloon
}

void TABProductionScene::initRecordVoiceSignal(){
    currentState = STATE_RECORD_VOICE_SIGNAL;
    soundPlayer.loadSound(recordVoiceSignalSoundfile);
    soundPlayer.play();
}

void TABProductionScene::initObjectScannerAway(){
    currentState = STATE_PRODUCTION_SCANNER_TO_AWAY;
    
    soundLevel = -100;
    
    objectTrajectory.clear();
    objectTrajectory.quadBezierTo(objectScannerPos, objectAwayPos, objectAwayPos,50);
    //
    objectsTween.setParameters(0, easinglinear, ofxTween::easeOut, 0, 1, 1000, 0); //TODO - time here (1000) should be set from somewhere else's constant, not hardcoded here
}

void TABProductionScene::objectCartToBelt(){
    float tweenVal = objectsTween.update();
    int trajectoryIndex = ofMap(tweenVal, 0, 1, 0, objectTrajectory.size()-1);
    productionObjects[0].setFromCenter(objectTrajectory.getVertices()[trajectoryIndex], productionObjects[0].width, productionObjects[0].height);
    
    float nextWidth = insideCartObject.width + (insideCartObject.width*0.6 - insideCartObject.width)*tweenVal;
    float nextHeight = insideCartObject.height + (insideCartObject.height*0.6 - insideCartObject.height)*tweenVal;
    
    productionObjects[0].setWidth(nextWidth);
    productionObjects[0].setHeight(nextHeight);

    
    if (objectsTween.isCompleted()){
        initObjectBeltToScanner();
    }
    
}
void TABProductionScene::objectBeltToScanner(){
    int trajectoryIndex = ofMap(objectsTween.update(), 0, 1, 0, objectTrajectory.size()-1);
    productionObjects[0].setFromCenter(objectTrajectory.getVertices()[trajectoryIndex], productionObjects[0].width, productionObjects[0].height);
    if (objectsTween.isCompleted() && !soundPlayer.getIsPlaying()){
        initRecordVoiceSignal();
        
    }
}

void TABProductionScene::recordVoiceSignal(){
    if (!soundPlayer.getIsPlaying()){
        initRecordVoice();
    }
}
void TABProductionScene::objectScannerAway(){
    int trajectoryIndex = ofMap(objectsTween.update(), 0, 1, 0, objectTrajectory.size()-1);
    productionObjects[0].setFromCenter(objectTrajectory.getVertices()[trajectoryIndex], productionObjects[0].width, productionObjects[0].height);
    
    if (objectsTween.isCompleted())
    {
        productionTestId.erase(productionTestId.begin());
        productionObjects.erase(productionObjects.begin());
        productionObjectsVoicePaths.erase(productionObjectsVoicePaths.begin());
        if (productionObjects.size() > 0){
            initObjectCartToBelt();
        }
        else {
            bCompleted = true;
            
        }
    }
    
    
}

void TABProductionScene::togglePause(){
    if (currentState != STATE_RECORD_VOICE){
        bPause = !bPause;
        if (bPause){
            lastState = currentState;
            currentState = STATE_PRODUCTION_IDLE;
        }else{
            currentState = lastState; 
        }
    }
}

