From here http://openframeworks.cc/download/ download the file linked in the ios - openFrameworks for xcode link.

Current repo needs to be put into *OF_ROOT/apps/myApps*



Debug/Test/Secret gestures
------

+ On app
Nome: admin
Età: 0, 1, 2, 3, 4
Starts from end of 0, 1, 2, 3, 4 level (for debugging end level animation).

Nome: admin
Età: (any except the above)
Makes all levels unblocked

On comprension scene three fingers tap start reproduction of instruction for which object to put into the cart.

+ Before compiling
Debug loggings and graphics are enabled/disabled by coommenting/uncommenting a preprocessor directive at the top of  *TABMainApp.h*


Third party OS libraries
-------

[Reachability](https://github.com/tonymillion/Reachability)
[JXLS](https://github.com/JanX2/JXLS)


Sounds
----------

Uses samples from:
[here](https://www.freesound.org/people/TeamMasaka/sounds/197432/)
[here](https://www.freesound.org/people/chripei/sounds/165492/)
[here](https://www.freesound.org/people/zerolagtime/sounds/144418/)
